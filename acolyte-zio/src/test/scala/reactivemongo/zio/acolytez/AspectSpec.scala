package reactivemongo.zio.acolytez

import acolyte.reactivemongo.QueryResponse
import reactivemongo.api.bson.{ BSONDocument, document }
import reactivemongo.api.{ ReadConcern, ReadPreference }
import reactivemongo.zio.{ CollectionName, Mongo, MongoDriver }

import zio.Ref
import zio.test.{ ZIOSpecDefault, assertTrue }
import zio.test.Spec

object AspectSpec extends ZIOSpecDefault {
  def spec: Spec[Any, Throwable] = suite("AspectSpec")(
    test("override read concern") {
      for {
        coll <- Mongo.collection(CollectionName("test"))
        c    <- Ref.make(List.empty[BSONDocument])
        _    <- TestConnection.setHandler(ConnectionHandlerZIO.fromQueryZIO { request =>
                  c.update(_ ++ request.body) as QueryResponse.count(42)
                })
        _    <- coll(Mongo.count(None))
        _    <- coll(Mongo.count(None)) @@ Mongo.aspects.readConcern(ReadConcern.Majority)
        body <- c.get.map(_.map(BSONDocument.pretty))
      } yield assertTrue(
        body.head.contains("""'level': 'local'"""),
        body.last.contains("""'level': 'majority'""")
      )
    },
    test("override read preference") {
      for {
        coll  <- Mongo.collection(CollectionName("test"))
        c     <- Ref.make(List.empty[BSONDocument])
        _     <- TestConnection.setHandler(ConnectionHandlerZIO.fromQueryZIO { request =>
                   c.update(_ ++ request.body) as QueryResponse.successful(document("_id" -> "foo"))
                 })
        stream = Mongo.findAll.stream[BSONDocument]() @@ Mongo.aspects.readPreference(ReadPreference.nearest)
        _     <- coll(stream).runCollect
        body  <- c.get.map(_.map(BSONDocument.pretty))
      } yield assertTrue(body.last.replaceAll("\\s+", "").contains("'$readPreference':{'mode':'nearest'}"))
    }
  ).provide(
    TestMongo.layer(),
    MongoDriver.layer
  )

}
