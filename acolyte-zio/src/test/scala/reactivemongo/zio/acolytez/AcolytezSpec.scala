package reactivemongo.zio.acolytez

import acolyte.reactivemongo.{ QueryResponse, WriteResponse }
import reactivemongo.api.bson.collection.BSONCollectionProducer
import reactivemongo.api.bson.{ BSONDocumentHandler, Macros, document }
import reactivemongo.zio._

import zio.ZLayer
import zio.test.TestAspect.timed
import zio.test._

object AcolytezSpec extends ZIOSpecDefault {
  case class Hello(_id: String)
  implicit val handler: BSONDocumentHandler[Hello] = Macros.handler[Hello]

  val collection = ZLayer(CollectionName("test").toCollection[BZONCollection])

  val alwaysResponse: ConnectionHandlerZIO = ConnectionHandlerZIO.fromConst(QueryResponse(document("_id" -> "world")))
  val alwaysWrite: ConnectionHandlerZIO    = ConnectionHandlerZIO.fromConst(WriteResponse(1))

  val spec =
    suite("Acolyte - Sanity")(
      test("Update the query handler") {
        for {
          _   <- TestConnection.setHandler(alwaysResponse)
          res <- Mongo.find(document("_id" -> "world")).one[Hello]
        } yield assertTrue(res.get == Hello("world"))
      },
      test("Update the write handler") {
        for {
          _   <- TestConnection.setHandler(alwaysWrite)
          res <- Mongo.insert.one(Hello("world2"))
        } yield assertTrue(res.ok)
      },
      test("resetting the handler") {
        for {
          _       <- TestConnection.setHandler(alwaysResponse)
          _       <- TestConnection.resetHandler
          handler <- TestConnection.handler
        } yield assertTrue(handler == ConnectionHandlerZIO.empty)
      }
    ).provideSome[MongoDriver](
      TestMongo.layer(),
      collection
    ).provideShared(MongoDriver.layer) @@ timed
}
