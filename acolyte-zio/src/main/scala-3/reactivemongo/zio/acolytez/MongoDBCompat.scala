package reactivemongo.zio.acolytez


import reactivemongo.acolyte.{ MessageHeader, Response, ResponseInfo, readReply }
import reactivemongo.api.bson.BSONDocument
import reactivemongo.io.netty.buffer.Unpooled
import reactivemongo.io.netty.channel.ChannelId

import scala.util.Try

trait MongoDBCompat {

  /**
   * Builds a response for a success query.
   *
   * @param chanId Unique ID of channel
   * @param docs BSON documents
   */
  def querySuccess(chanId: ChannelId, docs: Iterable[BSONDocument]): Try[Response] =
    mkResponse(chanId, 4 /* unspecified */, docs)

  /**
   * Builds a Mongo response.
   *
   * @param chanId Unique ID of channel
   * @param docs BSON documents
   */
  def mkResponse(chanId: ChannelId, flags: Int, docs: Iterable[BSONDocument]): Try[Response] = Try {
    import _root_.reactivemongo.api.bson.buffer.acolyte.{ writable, writeDocument }

    val body = writable()

    docs foreach (writeDocument(_, body))

    val len = 36 /* header size */ + body.size()
    val buf = Unpooled.buffer(len)

    buf.writeIntLE(len)
    buf.writeIntLE(System.identityHashCode(docs)) // fake response ID
    buf.writeIntLE(System.identityHashCode(buf))  // fake request ID
    buf.writeIntLE(4 /* OP_REPLY */ )            // opCode
    buf.writeIntLE(flags)
    buf.writeLongLE(0)                           // cursor ID
    buf.writeIntLE(0)                            // cursor starting from
    buf.writeIntLE(docs.size)                    // number of document
    buf.writeBytes(body.array())

    val in = Unpooled.wrappedUnmodifiableBuffer(buf)

    Response(MessageHeader(in), readReply(in), in, ResponseInfo(chanId))
  }

}