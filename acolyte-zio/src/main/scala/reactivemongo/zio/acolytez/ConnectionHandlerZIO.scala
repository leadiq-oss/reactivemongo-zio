package reactivemongo.zio.acolytez

import acolyte.reactivemongo._
import reactivemongo.acolyte.Response
import reactivemongo.io.netty.channel.ChannelId

import zio.{ IO, UIO, ZIO }

case class ConnectionHandlerZIO(
  queryHandler: QueryHandlerZIO = QueryHandlerZIO.empty,
  writeHandler: WriteHandlerZIO = WriteHandlerZIO.empty
) { self =>
  def withQueryHandlerZIO(handler: Request => UIO[PreparedResponse]): ConnectionHandlerZIO =
    copy(queryHandler = self.queryHandler.orElse(QueryHandlerZIO(handler)))

  def withQueryHandler(handler: Request => PreparedResponse): ConnectionHandlerZIO =
    copy(queryHandler = self.queryHandler.orElse(QueryHandlerZIO.fromQueryHandler(handler)))

  def withWriteHandlerZIO(handler: (WriteOp, Request) => UIO[PreparedResponse]): ConnectionHandlerZIO =
    copy(writeHandler = self.writeHandler.orElse(WriteHandlerZIO(handler)))

  def withWriteHandler(handler: (WriteOp, Request) => PreparedResponse): ConnectionHandlerZIO =
    copy(writeHandler = self.writeHandler.orElse(WriteHandlerZIO.fromWriteHandler(handler)))

  def ++(that: ConnectionHandlerZIO): ConnectionHandlerZIO =
    ConnectionHandlerZIO(
      self.queryHandler.orElse(that.queryHandler),
      self.writeHandler.orElse(that.writeHandler)
    )
}

object ConnectionHandlerZIO {
  lazy val empty: ConnectionHandlerZIO = ConnectionHandlerZIO()

  def fromConst(response: => PreparedResponse): ConnectionHandlerZIO =
    empty
      .withQueryHandler(_ => response)
      .withWriteHandler((_, _) => response)

  def fromQuery(handler: Request => PreparedResponse): ConnectionHandlerZIO =
    empty.withQueryHandler(handler)

  def fromQueryZIO(handler: Request => UIO[PreparedResponse]): ConnectionHandlerZIO =
    empty.withQueryHandlerZIO(handler)

  def fromWriteHandler(handler: (WriteOp, Request) => PreparedResponse): ConnectionHandlerZIO =
    empty.withWriteHandler(handler)

  def fromWriteHandlerZIO(handler: (WriteOp, Request) => UIO[PreparedResponse]): ConnectionHandlerZIO =
    empty.withWriteHandlerZIO(handler)
}

sealed trait QueryHandlerZIO { self =>
  def apply(chanId: ChannelId, query: Request): IO[Option[Throwable], Response]

  final def orElse(other: QueryHandlerZIO): QueryHandlerZIO = new QueryHandlerZIO {
    def apply(chanId: ChannelId, query: Request): IO[Option[Throwable], Response] =
      self(chanId, query).orElseOptional(other(chanId, query))
  }
}

object QueryHandlerZIO {

  def apply(handler: Request => UIO[PreparedResponse]): QueryHandlerZIO = new QueryHandlerZIO {
    def apply(chanId: ChannelId, query: Request): IO[Option[Throwable], Response] =
      handler(query).flatMap { pr =>
        ZIO.fromOption(pr(chanId)).flatMap(ZIO.fromTry(_).asSomeError)
      }
  }

  def fromQueryHandler(queryHandler: QueryHandler): QueryHandlerZIO = new QueryHandlerZIO {
    def apply(chanId: ChannelId, query: Request): IO[Option[Throwable], Response] =
      ZIO.fromOption(queryHandler(chanId, query)).flatMap(ZIO.fromTry(_).asSomeError)
  }

  lazy val empty: QueryHandlerZIO = QueryHandlerZIO(_ => ZIO.succeed(QueryResponse.undefined))
}

sealed trait WriteHandlerZIO { self =>
  def apply(chanId: ChannelId, op: WriteOp, query: Request): IO[Option[Throwable], Response]

  final def orElse(other: WriteHandlerZIO): WriteHandlerZIO = new WriteHandlerZIO {
    def apply(chanId: ChannelId, op: WriteOp, query: Request): IO[Option[Throwable], Response] =
      self(chanId, op, query).orElseOptional(other(chanId, op, query))
  }
}

object WriteHandlerZIO {
  def apply(handler: (WriteOp, Request) => UIO[PreparedResponse]): WriteHandlerZIO = new WriteHandlerZIO {
    def apply(chanId: ChannelId, op: WriteOp, request: Request): IO[Option[Throwable], Response] =
      handler(op, request).flatMap { pr =>
        ZIO.fromOption(pr(chanId)).flatMap(ZIO.fromTry(_).asSomeError)
      }
  }

  def fromWriteHandler(writeHandler: WriteHandler): WriteHandlerZIO = new WriteHandlerZIO {
    def apply(chanId: ChannelId, op: WriteOp, query: Request): IO[Option[Throwable], Response] =
      ZIO.fromOption(writeHandler(chanId, op, query)).flatMap(ZIO.fromTry(_).asSomeError)
  }

  lazy val empty: WriteHandlerZIO = WriteHandlerZIO((_, _) => ZIO.succeed(QueryResponse.undefined))
}
