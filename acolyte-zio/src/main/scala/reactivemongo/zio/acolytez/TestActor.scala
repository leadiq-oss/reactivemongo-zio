package reactivemongo.zio.acolytez

import scala.util.Success

import acolyte.reactivemongo.{ MongoDB => _, _ }
import reactivemongo.acolyte.{ Query => RQuery, _ }
import reactivemongo.api.bson.{ BSONArray, BSONDocument, BSONString, BSONValue }
import reactivemongo.core.protocol.ProtocolMetadata
import reactivemongo.io.netty.channel.{ ChannelId, DefaultChannelId }

import zio.{ Task, Trace, URIO, Unsafe, ZIO }
import reactivemongo.api.MongoConnectionOptions
import reactivemongo.core.netty.ChannelFactory

class TestActor(runtime: zio.Runtime[TestConnection]) extends MongoDBSystem {

  lazy val initialAuthenticates: Seq[Authenticate] = Nil

  protected def authReceive: PartialFunction[Any, Unit] = { case _ => () }

  val supervisor              = "Acolytez"
  val name                    = "AcolytezConnection"
  lazy val seeds: Seq[String] = Nil

  lazy val options: MongoConnectionOptions = reactivemongo.api.MongoConnectionOptions()

  protected def sendAuthenticate(connection: Connection, authentication: Authenticate): Connection =
    connection

  protected def newChannelFactory(effect: Unit): ChannelFactory =
    reactivemongo.acolyte.channelFactory(supervisor, name, options)

  private def handleWrite(chanId: ChannelId, op: WriteOp, req: Request)(implicit
    trace: Trace
  ): URIO[TestConnection, Option[Response]] =
    TestConnection.handler
      .flatMap(h =>
        ZIO
          .suspend(
            h.writeHandler(chanId, op, req)
              .unsome
              .catchAll(e =>
                ZIO.some(
                  MongoDB
                    .writeError(chanId, Option(e.getMessage).getOrElse(e.getClass.getName))
                    .getOrElse(MongoDB.mkWriteError(chanId))
                )
              )
          )
          .catchAll[TestConnection, Nothing, Option[Response]](cause =>
            ZIO.some(invalidWriteHandler(chanId, cause.getMessage))
          )
      )

  override lazy val receive: Receive = {
    case msg @ ExpectingResponse(
          RequestMaker(RQuery(_ /*flags*/, coln, _ /*off*/, _ /* len */ ), doc, _ /*pref*/, chanId),
          promise
        ) =>
      val cid = chanId getOrElse newChannelId()
      val req = Request(coln, doc)

      val resp = req match {
        case Request(_, SimpleBody((k @ WriteQuery(op), BSONString(cn)) :: es)) if (coln.endsWith(f".$$cmd")) =>
          val opBody: Option[List[BSONDocument]] = if (k == "insert") {
            es.collectFirst { case ("documents", a: BSONArray) =>
              a.values.toList.collect { case d: BSONDocument => d }
            }
          } else {
            val Key = k + "s"

            es.collectFirst { case (Key, a: BSONArray) =>
              a.values.toList.collect { case d: BSONDocument => d }
            }
          }

          val wreq = new Request {
            val collection = coln.dropRight(4) + cn
            val body       = opBody.getOrElse(List.empty)
          }

          handleWrite(cid, op, wreq).map(
            _.getOrElse(
              noWriteResponse(cid, msg.toString)
            )
          )

        case Request(collName, SimpleBody(ps)) =>
          val reqBody: List[BSONDocument] =
            ps.foldLeft(
              Option.empty[BSONDocument] -> (List.empty[(String, BSONValue)])
            ) {
              case ((_, opts), ("$query", q: BSONDocument)) => Some(q) -> opts
              case ((q, opts), opt)                         => q       -> (opt +: opts)
            } match {
              case (Some(q), opts) => List(q, BSONDocument(opts.reverse))
              case (_, opts)       => List(BSONDocument(opts.reverse))
            }

          val qreq = new Request {
            val collection = collName
            val body       = reqBody
          }

          TestConnection.handler.flatMap { h =>
            ZIO
              .suspend(
                h.queryHandler(cid, qreq)
                  .mapError(
                    _.map(e =>
                      MongoDB
                        .queryError(cid, Option(e.getMessage).getOrElse(e.getClass.getName))
                        .getOrElse(MongoDB.mkQueryError(cid))
                    )
                  )
                  .flattenErrorOption(noQueryResponse(cid, msg.toString))
                  .merge
              )
              .catchAll[Any, Nothing, Response](cause => ZIO.succeed(invalidQueryHandler(cid, cause.getMessage)))
          }

        case _ => ???
      }

      promise.completeWith(
        Unsafe.unsafe(implicit u =>
          runtime.unsafe.runToFuture(
            resp.flatMap(r => r.error.fold[Task[Response]](ZIO.succeed(r))(ZIO.fail(_)))
          )
        )
      )
    case _: RegisterMonitor =>
      val s = sender()
      // TODO: configure protocol metadata
      s ! PrimaryAvailable(ProtocolMetadata.Default)
      s ! SetAvailable(ProtocolMetadata.Default)

    case Close() =>
      sender() ! Closed

      try postStop()
      catch {
        case _: java.util.concurrent.TimeoutException =>
          logger.warn("Fails to close in a timely manner the MongoDBSystem")
      }

    case msg @ _ =>
      //next forward msg
      ()
  }

  // ---

  private def newChannelId(): ChannelId = DefaultChannelId.newInstance()

  // Write operations sent as `Query`
  private object WriteQuery {
    def unapply(repr: String): Option[WriteOp] = repr match {
      case "insert" => Some(InsertOp)
      case "update" => Some(UpdateOp)
      case "delete" => Some(DeleteOp)
      case _        => None
    }
  }

  // Fallback response when no handler provides a query response.
  @inline private def noQueryResponse(chanId: ChannelId, req: String): Response =
    MongoDB.queryError(chanId, s"No response: $req") match {
      case Success(resp) => resp
      case _             => MongoDB.mkQueryError(chanId)
    }

  // Fallback response when write handler is failing.
  @inline private def invalidWriteHandler(chanId: ChannelId, msg: String): Response =
    MongoDB.writeError(chanId, s"Invalid write handler: $msg") match {
      case Success(resp) => resp
      case _             => MongoDB.mkWriteError(chanId)
    }

  // Fallback response when no handler provides a write response.
  @inline private def noWriteResponse(chanId: ChannelId, req: String): Response =
    MongoDB.writeError(chanId, s"No response: $req") match {
      case Success(resp) => resp
      case _             => MongoDB.mkWriteError(chanId)
    }

  // Fallback response when query handler is failing.
  @inline private def invalidQueryHandler(chanId: ChannelId, msg: String): Response =
    MongoDB.queryError(chanId, s"Invalid query handler: $msg") match {
      case Success(resp) => resp
      case _             => MongoDB.mkQueryError(chanId)
    }
}
