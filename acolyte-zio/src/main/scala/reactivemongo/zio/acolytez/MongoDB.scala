package reactivemongo.zio.acolytez

import scala.util.Try

import acolyte.reactivemongo.{ DeleteOp, InsertOp, UpdateOp, WriteOp }
import reactivemongo.acolyte._
import reactivemongo.api.bson.BSONDocument
import reactivemongo.io.netty.buffer.Unpooled
import reactivemongo.io.netty.channel.{ ChannelId, DefaultChannelId }

/* MongoDB companion */
object MongoDB extends MongoDBCompat {

  /**
   * Builds a response with error details for a query.
   *
   * @param chanId Unique ID of channel
   * @param error Error message
   */
  private[acolytez] def queryError(chanId: ChannelId, error: String, code: Option[Int] = None): Try[Response] =
    mkResponse(
      chanId,
      1 /*QueryFailure*/, {
        val doc = BSONDocument("$err" -> error)
        code.fold(Seq(doc))(c => Seq(doc ++ ("code" -> c)))
      }
    )

  /**
   * Builds a response with error details for a write operation.
   *
   * @param chanId Uniqe ID of channel
   * @param error Error message
   * @param code Error code
   */
  private[acolytez] def writeError(chanId: ChannelId, error: String, code: Option[Int] = None): Try[Response] =
    mkResponse(
      chanId,
      4 /* unspecified */,
      List(
        BSONDocument(
          "ok"              -> 0,
          "err"             -> error,
          "errmsg"          -> error,
          "code"            -> code.getOrElse(-1),
          "updatedExisting" -> false,
          "n"               -> 0
        )
      )
    )

  /**
   * Builds a response for a successful write operation.
   *
   * @param chanId Unique ID of channel
   * @param count The number of documents affected by last command, 0 if none
   * @param updatedExisting Some existing document has been updated
   */
  private[acolytez] def writeSuccess(chanId: ChannelId, count: Int, updatedExisting: Boolean = false): Try[Response] =
    mkResponse(
      chanId,
      4 /*unspecified*/,
      List(BSONDocument("ok" -> 1, "updatedExisting" -> updatedExisting, "n" -> count))
    )

  /** Defines instance of WriteOp enum. */
  @inline def writeOp(mongoOp: WriteRequestOp): Option[WriteOp] =
    mongoOp match {
      case Delete(_, _) => Some(DeleteOp)
      case Insert(_, _) => Some(InsertOp)
      case Update(_, _) => Some(UpdateOp)
    }

  private[acolytez] def mkQueryError(chanId: ChannelId = DefaultChannelId.newInstance()): Response = mkError(
    chanId,
    Array[Byte](76, 0, 0, 0, 16, -55, -63, 115, -49, 116, 119, 55, 4, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 1, 0, 0, 0, 40, 0, 0, 0, 2, 36, 101, 114, 114, 0, 25, 0, 0, 0, 70, 97, 105, 108, 115, 32, 116, 111, 32, 99,
      114, 101, 97, 116, 101, 32, 114, 101, 115, 112, 111, 110, 115, 101, 0, 0)
  ) // "Fails to create response"

  private[acolytez] def mkWriteError(chanId: ChannelId = DefaultChannelId.newInstance()): Response = mkError(
    chanId,
    Array[Byte](-126, 0, 0, 0, -29, 50, 14, 73, -115, -6, 46, 67, 4, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 1, 0, 0, 0, 94, 0, 0, 0, 16, 111, 107, 0, 0, 0, 0, 0, 2, 101, 114, 114, 0, 25, 0, 0, 0, 70, 97, 105, 108,
      115, 32, 116, 111, 32, 99, 114, 101, 97, 116, 101, 32, 114, 101, 115, 112, 111, 110, 115, 101, 0, 2, 101, 114,
      114, 109, 115, 103, 0, 25, 0, 0, 0, 70, 97, 105, 108, 115, 32, 116, 111, 32, 99, 114, 101, 97, 116, 101, 32, 114,
      101, 115, 112, 111, 110, 115, 101, 0, 16, 99, 111, 100, 101, 0, -1, -1, -1, -1, 0)
  )

  @inline private def mkError(chanId: ChannelId, docs: Array[Byte]): Response = {
    val buf = Unpooled.wrappedUnmodifiableBuffer(Unpooled.copiedBuffer(docs))

    Response(MessageHeader(buf), readReply(buf), buf, ResponseInfo(chanId))
  }
}
