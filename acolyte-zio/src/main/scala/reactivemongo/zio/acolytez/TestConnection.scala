package reactivemongo.zio.acolytez

import acolyte.reactivemongo.{ PreparedResponse, Request, WriteOp }
import zio._

trait TestConnection {

  /**
   * Modifies the current connection handler producing a new connection handler and optionally a value
   */
  def modifyHandler[A](f: ConnectionHandlerZIO => (A, ConnectionHandlerZIO))(implicit trace: Trace): UIO[A]

  def getHandler(implicit trace: Trace): UIO[ConnectionHandlerZIO] = modifyHandler(conn => (conn, conn))

  /**
   * Sets the current connection handler to the provided handler effectively overriding any existing behavior
   */
  def setHandler(handler: ConnectionHandlerZIO)(implicit trace: Trace): UIO[Unit] =
    modifyHandler(_ => ((), handler))
}

object TestConnection {

  def make(
    connectionHandler: ConnectionHandlerZIO = ConnectionHandlerZIO.empty
  )(implicit trace: Trace): URIO[Scope, TestConnection] =
    Ref
      .make(connectionHandler)
      .map(ref =>
        new TestConnection {
          def modifyHandler[A](f: ConnectionHandlerZIO => (A, ConnectionHandlerZIO))(implicit trace: Trace): UIO[A] =
            ref.modify(f)
        }
      )

  def handler(implicit trace: Trace): ZIO[TestConnection, Nothing, ConnectionHandlerZIO] =
    ZIO.serviceWithZIO[TestConnection](_.getHandler)

  def handleWrite(handler: (WriteOp, Request) => PreparedResponse)(implicit
    trace: Trace
  ): ZIO[TestConnection, Nothing, Unit] =
    updateHandler(_.withWriteHandler(handler))

  def handleQuery(handler: Request => PreparedResponse)(implicit trace: Trace): ZIO[TestConnection, Nothing, Unit] =
    updateHandler(_.withQueryHandler(handler))

  def handleQueryZIO[R](
    f: Request => URIO[R, PreparedResponse]
  )(implicit trace: Trace): ZIO[TestConnection & R, Nothing, Unit] =
    ZIO.environmentWithZIO[R](r => updateHandler(_.withQueryHandlerZIO(f(_).provideEnvironment(r))))

  def handleWriteZIO[R](
    f: (WriteOp, Request) => URIO[R, PreparedResponse]
  )(implicit trace: Trace): ZIO[TestConnection & R, Nothing, Unit] =
    ZIO.environmentWithZIO[R](r => updateHandler(_.withWriteHandlerZIO(f(_, _).provideEnvironment(r))))

  def setHandler(handler: ConnectionHandlerZIO)(implicit trace: Trace): ZIO[TestConnection, Nothing, Unit] =
    ZIO.serviceWithZIO[TestConnection](_.setHandler(handler))

  def resetHandler(implicit trace: Trace): ZIO[TestConnection, Nothing, Unit] =
    setHandler(ConnectionHandlerZIO.empty)

  def updateHandler(f: ConnectionHandlerZIO => ConnectionHandlerZIO)(implicit
    trace: Trace
  ): ZIO[TestConnection, Nothing, Unit] =
    ZIO.serviceWithZIO[TestConnection](_.modifyHandler(conn => ((), f(conn)))).unit
}
