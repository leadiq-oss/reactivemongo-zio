package reactivemongo.zio.acolytez

import java.util.concurrent.TimeUnit
import scala.concurrent.duration.FiniteDuration
import org.apache.pekko.actor.{ ActorRef, ActorSystem, Props }
import reactivemongo.acolyte.MongoConnection
import reactivemongo.api.{ DB, MongoConnectionOptions }
import reactivemongo.zio.{ Mongo, MongoDriver }
import zio.{ System => _, _ }

object TestMongo {
  def layer(
    initialHandler: ConnectionHandlerZIO = ConnectionHandlerZIO.empty,
    options: MongoConnectionOptions = MongoConnectionOptions.default
  )(implicit trace: Trace): ZLayer[MongoDriver, Throwable, Mongo & TestConnection] =
    ZLayer.scopedEnvironment[MongoDriver](make(initialHandler, options))

  def make(
    initialHandler: ConnectionHandlerZIO,
    connectionOptions: MongoConnectionOptions = MongoConnectionOptions.default
  )(implicit
    trace: Trace
  ): ZIO[Scope & MongoDriver, Throwable, ZEnvironment[Mongo & TestConnection]] = for {
    runtime  <- ZIO.runtime[Any]
    system   <- MongoDriver.driverSystem
    testConn <- TestConnection.make(initialHandler)
    actorRef <- ZIO.attempt(system.actorOf(Props(classOf[TestActor], runtime.withEnvironment(ZEnvironment(testConn)))))
    conn     <- makeConnection(actorRef, system, connectionOptions)
    mongo     = makeMongo(conn)
  } yield ZEnvironment[Mongo, TestConnection](mongo, testConn)

  private def makeConnection(
    actorRef: ActorRef,
    system: ActorSystem,
    options: MongoConnectionOptions
  ): ZIO[Scope, Throwable, MongoConnection] =
    ZIO
      .attempt(
        new MongoConnection(
          s"supervisor-${System.identityHashCode(actorRef)}",
          s"connection-${System.identityHashCode(actorRef)}",
          system,
          actorRef,
          options
        )
      )
      .withFinalizer(c =>
        ZIO
          .fromFuture(implicit ec => c.close()(FiniteDuration(10, TimeUnit.SECONDS)))
          .orDie
          .timeout(10.seconds)
      )

  private def makeMongo(conn: MongoConnection) =
    new Mongo {
      def database(implicit trace: Trace): Task[DB] =
        ZIO.fromFuture(implicit ec => conn.database("acolytez"))
    }

}
