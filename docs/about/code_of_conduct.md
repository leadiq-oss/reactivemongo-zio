---
id: about_coc
title:  "reactivemongo-zio Code of Conduct"
---

We are committed to providing a friendly, safe and welcoming
environment for all, regardless of level of experience, gender, gender
identity and expression, sexual orientation, disability, personal
appearance, body size, race, ethnicity, age, religion, nationality, or
other such characteristics.

The reactivemongo-zio project follows the [Scala Code of Conduct](https://www.scala-lang.org/conduct/).
All participants, contributors and members are expected to follow the Scala Code of Conduct when discussing 
the project on the available communication channels. If you are being harassed, please contact us 
immediately so that we can support you.

