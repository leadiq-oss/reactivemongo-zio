---
id: overview_basic_operations
title: "Contents"
---

## Setup database connection

In order to query mongo you will need to first establish a connection with the database. This can be done in a number of ways.

These easiest is to use one of the factory methods available on the `Mongo` companion object. These methods provide the best "out of the box" experience and are recommended for most use cases if you 
will only have a single database connection.

```scala
// If you have a single hard coded string for the connection you can use
Mongo.fromConnectionStringScoped("mongodb://localhost:27017")

// Or
Mongo.fromConnectionString("mongodb://localhost:27017")
```

The former will produce a scoped effect that will return a `Mongo` instance using the default DB name. The latter will produce a the same but wrapped in a Layer for easier composition with your
startup application.

If you need to load your connection string from else where, say a configuration file or the command line, you can use:
```scala
// If you have a Config type that is part of the R environment
Mongo.fromEnvironmentConfigScoped[MyConfig](_.mongo.uri)
Mongo.fromEnvironmentConfig[MyConfig](_.mongo.uri)

// Or if you are using the built in ConfigProvider
Mongo.fromConfig
Mongo.fromConfigScoped
Mongo.fromConfig(myConfig)(c => MongoConfig(c.url, None))
Mongo.fromConfigScoped(myConfig)(c => MongoConfig(c.url, None))
```
As with above, the variants ending in `*Scoped` will produce scoped effects while the others will produce a Layer.

If you need more control over the connection or you want to reuse aspects of the connection you can also provide the component separately.
Under the hood `Mongo` needs three components: a `MongoDriver`, a `MongoConnection` and a `MongoDatabase`. These can be provided separately and composed together to create a `Mongo` instance.

```scala
program.provide(
  MongoDriver.layer,
  Connection.fromConfig,
  Mongo.layer
)
```

The `Connection` object provides the same variants as the `Mongo` object for creating connections. But by separating them you 
can also reuse the connection or the driver across multiple databases. Likewise, the `Mongo` `layer*` and `make*` variants allow you to
specify the database name to use, for the parameterless variants the default database name from the connection uri will be used.


## Basic operations with Mongo

The `Mongo` companion object contains all of the mongo operators that you can use to interact with the database. These operators can fluently be composed into larger expressions.

### Querying documents

Basic document queries are done using the `find*` variants. These methods usually take a query object as a parameter as well as a projection,
the returned object is then used to define the response type.

```scala
Mongo.find(document("_id" -> 42))
Mongo.findAll // Short hand for Mongo.find(document)
Mongo.find(UserById(42)) // If `UserById` has an implicit `BSONDocumentWriter` in scope
Mongo.findWith(document("_id" -> 42))(_.skip(10).sort(document("name" -> 1))) // Apply query modifiers
```

Once you have specified a query object you then need to specify the response object. You have three possible variants here

```scala
// Returns a single optional document where `User` has a `BSONDocumentReader` in scope
Mongo.find(document("_id" -> 42)).one[User]

// Returns a stream of documents where `User` has a `BSONDocumentReader` in scope
Mongo.find(document("_id" -> 42)).stream[User]()

// Returns a single _required_ document
Mongo.find(document("_id" -> 42)).requireOne[User]
```

In particular the `stream` method returns a `ZStream` which automatically handles the cursor management for you. And because it is a `ZStream`
you can use all of the combinators available on `ZStream` to manipulate the response stream.

### Working with collections

You may have noticed that the above examples are returning `ZIO` objects that require a `BZONCollection` in their environment parameter. 
In order to run these effects you will need to specify the collection that they are to run on. You can do this in a number of ways depending on your use cases.

If you are running in a small ZIO application where you are inlining all of your services you can use the `collection*` methods on the `Mongo` companion object:

```scala
Mongo.collectionWithZIO("users") {
  Mongo.find(document("_id" -> 42)).one[User]
}

for {
  userCollection <- Mongo.collection("users")
  _ <- userCollection(Mongo.insert.one(User(id)).ignore)
} yield ()
```

If you will be using the Module 2.0 pattern and injecting the instance of `mongo` into your service dependencies, it may be more convenient to use the `collection` method on the `Mongo` instance itself.

```scala
class ServiceDep(mongo: Mongo) {
  val users = mongo.collection("users")
  
  
  def findUser(id: Int) =
    users(Mongo.find(document("_id" -> id)).one[User])
  
}
```

### Inserting/Updating/Deleting documents

The insertion, updating and deletion of documents is done using the `insert`, `update` and `delete` methods on the `Mongo` companion object.
They all use the same pattern of providing a fluent interface that specifies the type of modification followed by a `one` or `many` method to actually perform the operation.

```scala
import reactivemongo.zio.Mongo

Mongo.insert.one(User(id)).ignore
Mongo.delete.one(UserById(id)).ignore
Mongo.update.one(UserById(id), document("$set" -> document("name" -> "John"))).ignore
```


```scala
val getUser(id: Int) = Mongo.find(document("_id" -> id)).one[User]

// You can apply a collection to a regional set of queries
Mongo.collection("users") {
  getUser(42) <*> getUser(43)
}

// Or to a specific operation
getUser(42).collection("users")

// If you need to pass around the collection construct a `CollectionName` object,
// this can then be used when providing collections
val collection = CollectionName("users").asCollection[BZONCollection].toLayer

getUser(42).provide(collection)

// With ZIO 1 we can use the `>>>` operator to pass an effect as a layer
(Mongo.collectionService("users").orDie >>> getUser(42))
  // In a typical application you would just do this once at the end of the world.
  .provideLayer(liveMongo)
```

Here we assume that we have a class `User` set up with a `BSONDocumentHandler` as in `reactivemongo`.

```scala
import reactivemongo.api.bson.{ BSONDocumentHandler, Macros }
import reactivemongo.api.bson.Macros.Annotations.Key

case class User(@Key("_id") id: Int, name: Option[String] = None)

object User {
  implicit val handler: BSONDocumentHandler[User] = Macros.handler[User]
}
```

## Composing with ZIO Effects

Note that the above operations offered by `Mongo` returns a ZIO effect - hence they can be composed
to form larger expressions:

```scala
// returns an effect
def addUser(id: Int): URIO[BZONCollection, Unit] =
  Mongo.insert.one(User(id)).ignore

// compose
addUser(42) *> Mongo.find(document("_id" -> 42)).one[User]
```
          
Another example of composing operations to form larger expressions using the power of ZIO effects:

```scala
Mongo.insert.many(
  List.fill(5)(document("a" -> "b")) ++ List.fill(5)(document("a" -> "c"))
) *> Mongo.count(document("a" -> "b"))
```

Here are some more advanced examples of how to compose effects along with streaming queries in Mongo
using `reactivemongo-zio`:

```scala
Mongo
  .find(document)
  .stream[User](100, Cursor.DoneOnError())
  .runCollect
  .map(result =>
    // use result
  )
```
