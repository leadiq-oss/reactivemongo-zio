---
id: overview_index
title: "Contents"
---

## Installation

Add the library to your `build.sbt` dependencies:

`libraryDependencies += "com.leadiq" %% "reactivemongo-zio" % "2.0.0-RC4"`

## Quickstart

```scala
import reactivemongo.zio._

object MongoApp extends ZIOAppDefault {
  
  case class Author(_id: Long, name: String, hobbies: List[String])
  case class Books(@Key("_id") isbn: String, title: String, authorId: Long, published: LocalDate, checked_out: Boolean = false)
  
  implicit val authorHandler = Macros.handler[Author]
  implicit val booksHandler = Macros.handler[Books]
  
  val authors = List(
    Author(1, "John Grisham" , List("Reading", "Writing")),
    Author(2, "Jane Austen" , List("Poetry", "Dancing")),
    Author(3, "Jack London" , List("Hiking", "Camping"))
  )
  
  val books = List(
    Book("978-0-553-57340-3" , "The Firm" , 1 , LocalDate.of(1991, 1, 1)),
    Book("978-0-553-57341-0" , "The Pelican Brief" , 1 , LocalDate.of(1992, 1, 1)),
    Book("978-0-553-57342-7" , "The Client" , 1 , LocalDate.of(1993, 1, 1))
  )
  
  val run = (for {
    authorsCollection <- Mongo.collection("authors")
    booksCollection <- Mongo.collection("books")
    
    // Insert some data
    _ <- authorsCollection {
      ZIO.foreachDiscard(authors)(Mongo.insert.one(_))
    }
    _ <- booksCollection {
      ZIO.foreachDiscard(books)(Mongo.insert.one(_))
    }
    
    johnGrisham <- authorsCollection {
      Mongo.find(document("name" -> "John Grisham")).one[Author]
    }
    // Find all books by John Grisham and check them all out
    written <- booksCollection {
      Mongo.find(document("authorId" -> johnGrisham._id))
        .stream[Book]()
        .tap {
          book => Mongo.update.one(document("_id" -> book.isbn), document("$set" -> document("checked_out" -> true)))
        }
    }
  } yield ()).provide(Mongo.fromConfig)
  
}

```
