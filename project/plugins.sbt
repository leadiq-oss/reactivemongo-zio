resolvers += Resolver.bintrayIvyRepo("rallyhealth", "sbt-plugins")

addSbtPlugin("ch.epfl.scala"                     % "sbt-bloop"                 % "1.4.8")
addSbtPlugin("ch.epfl.scala"                     % "sbt-scalafix"              % "0.13.0")
addSbtPlugin("com.eed3si9n"                      % "sbt-buildinfo"             % "0.10.0")
addSbtPlugin("com.eed3si9n"                      % "sbt-unidoc"                % "0.4.3")
addSbtPlugin("com.github.cb372"                  % "sbt-explicit-dependencies" % "0.2.16")
addSbtPlugin("com.thoughtworks.sbt-api-mappings" % "sbt-api-mappings"          % "3.0.0")
addSbtPlugin("de.heikoseeberger"                 % "sbt-header"                % "5.6.5")
addSbtPlugin("org.scalameta"                     % "sbt-mdoc"                  % "2.2.21")
addSbtPlugin("org.scalameta"                     % "sbt-scalafmt"              % "2.4.2")
addSbtPlugin("pl.project13.scala"                % "sbt-jcstress"              % "0.2.0")
addSbtPlugin("pl.project13.scala"                % "sbt-jmh"                   % "0.4.3")
addSbtPlugin("com.rallyhealth.sbt"               % "sbt-git-versioning"        % "1.6.0")
addSbtPlugin("org.xerial.sbt"                    % "sbt-sonatype"              % "3.9.12")
addSbtPlugin("com.jsuereth"                      % "sbt-pgp"                   % "2.1.1")
