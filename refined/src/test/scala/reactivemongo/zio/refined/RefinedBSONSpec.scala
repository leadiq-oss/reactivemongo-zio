package reactivemongo.zio.refined

import eu.timepit.refined.types.all.NonEmptyString
import eu.timepit.refined.types.numeric.NonNegInt
import reactivemongo.api.bson.{ BSON, BSONInteger, BSONString }

import zio.test.Assertion.{ equalTo, isFailure, isSuccess }
import zio.test.{ TestAspect, ZIOSpecDefault, assert }
import zio.Scope
import zio.test.{ Spec, TestEnvironment }

object RefinedBSONSpec extends ZIOSpecDefault with RefinedBSON {

  val spec: Spec[Environment with TestEnvironment with Scope, Any] = suite("RefinedBSON")(
    test("can encode and decode refined Strings") {
      assert(BSON.write(NonEmptyString.apply("foo")))(isSuccess(equalTo(BSONString("foo")))) &&
      assert(BSON.read[NonEmptyString](BSONString("bar")))(isSuccess(equalTo(NonEmptyString.apply("bar"))))
    },
    test("can encode and decode refined Ints") {
      assert(BSON.write(NonNegInt(42)))(isSuccess(equalTo(BSONInteger(42)))) &&
      assert(BSON.read[NonNegInt](BSONInteger(42)))(isSuccess(equalTo(NonNegInt(42))))
    },
    test("rejects invalid data") {
      assert(BSON.read[NonNegInt](BSONInteger(-100)))(isFailure) &&
      assert(BSON.read[NonEmptyString](BSONString("")))(isFailure)
    }
  ) @@ TestAspect.scala2Only

}
