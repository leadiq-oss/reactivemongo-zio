package reactivemongo.zio.refined

import eu.timepit.refined.api.{ Refined, Validate }
import eu.timepit.refined.refineV
import reactivemongo.api.bson.{ BSONHandler, BSONReader, BSONWriter }

trait RefinedBSON {

  implicit def refinedHandler[T, P](implicit
    validate: Validate[T, P],
    tReader: BSONReader[T],
    tWriter: BSONWriter[T]
  ): BSONHandler[Refined[T, P]] = BSONHandler.from(
    bson =>
      tReader
        .readTry(bson)
        .flatMap(refineV[P](_).left.map(new IllegalArgumentException(_)).toTry),
    t => tWriter.writeTry(t.value)
  )

}
