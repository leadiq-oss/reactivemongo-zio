package reactivemongo.zio

import reactivemongo.api.{ FailoverStrategy, ReadConcern, ReadPreference, WriteConcern }

import zio.{ Scope, ZIO }

trait Modifiers {

  def readPreferenceScoped(rp: ReadPreference): ZIO[Scope, Nothing, Unit] =
    MongoOverrideContext.overrideMongoContext.locallyScopedWith(_.copy(readPreference = Some(rp)))

  def readConcernScoped(rc: ReadConcern): ZIO[Scope, Nothing, Unit] =
    MongoOverrideContext.overrideMongoContext.locallyScopedWith(_.copy(readConcern = Some(rc)))

  def writeConcernScoped(wc: WriteConcern): ZIO[Scope, Nothing, Unit] =
    MongoOverrideContext.overrideMongoContext.locallyScopedWith(_.copy(writeConcern = Some(wc)))

  def failoverStrategyScoped(fs: FailoverStrategy): ZIO[Scope, Nothing, Unit] =
    MongoOverrideContext.overrideMongoContext.locallyScopedWith(_.copy(failoverStrategy = Some(fs)))

}
