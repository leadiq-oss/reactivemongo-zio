package reactivemongo.zio

import reactivemongo.api.bson.collection.BSONSerializationPack.Writer
import reactivemongo.api.commands.WriteResult
import reactivemongo.core.errors.ReactiveMongoException
import reactivemongo.zio.MongoOverrideContext.writeConcernWith
import reactivemongo.zio.results.MultiBulkWriteResult

import zio.{ Trace, URIO, ZIO }

final case class InsertOp private[zio] (
  ordered: Boolean
) {

  private val builder: URIO[BZONCollection, BZONCollection#InsertBuilder] =
    writeConcernWith(wc => ZIO.serviceWith[BZONCollection](b => wc.fold(b.insert(ordered))(b.insert(ordered, _))))

  def one[T: Writer](document: T)(implicit trace: Trace): ZIO[BZONCollection, ReactiveMongoException, WriteResult] =
    builder
      .flatMap(b => ZIO.fromFuture(implicit ec => b.one(document)))
      .refineOrDie { case e: ReactiveMongoException => e }

  def many[T: Writer](
    documents: Iterable[T]
  )(implicit trace: Trace): ZIO[BZONCollection, ReactiveMongoException, MultiBulkWriteResult]                      =
    builder
      .flatMap(b => ZIO.fromFuture(implicit ec => b.many(documents)))
      .map(MultiBulkWriteResult.apply)
      .refineOrDie { case e: ReactiveMongoException => e }

}
