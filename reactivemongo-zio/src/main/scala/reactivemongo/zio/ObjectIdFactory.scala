package reactivemongo.zio

import java.util.concurrent.TimeUnit

import reactivemongo.api.bson.BSONObjectID

import zio.{ Clock, FiberRef, Scope, Trace, UIO, Unsafe, ZIO }

/**
 * A service trait to determine the implementation of the ObjectId generation.
 * By default this will use the system clock plus some random bytes, however it can be overridden
 *  Using `withObjectId` and `withObjectIdScoped`
 */
trait ObjectIdFactory {
  def generate(fillOnlyTimestamp: Boolean = false)(implicit trace: Trace): UIO[BSONObjectID]
}

object ObjectIdFactory {

  val default: ObjectIdFactory = new ObjectIdFactory {
    def generate(fillOnlyTimestamp: Boolean = false)(implicit trace: Trace): UIO[BSONObjectID] =
      Clock.currentTime(TimeUnit.MILLISECONDS).map(BSONObjectID.fromTime(_, fillOnlyTimestamp))
  }

  private[zio] val currentObjectIdFactory: FiberRef[ObjectIdFactory] =
    Unsafe.unsafe(implicit u => FiberRef.unsafe.make[ObjectIdFactory](default))

  def generate(fillOnlyTimestamp: Boolean = false)(implicit trace: Trace): UIO[BSONObjectID] =
    currentObjectIdFactory.getWith(_.generate(fillOnlyTimestamp))

  def withObjectId[R, E, A <: ObjectIdFactory, B](factory: => A)(zio: ZIO[R, E, B]): ZIO[R, E, B] =
    currentObjectIdFactory.locallyWith(_ => factory)(zio)

  def withObjectIdScoped[A <: ObjectIdFactory](factory: => A): ZIO[Scope, Nothing, Unit] =
    currentObjectIdFactory.locallyScopedWith(_ => factory)
}
