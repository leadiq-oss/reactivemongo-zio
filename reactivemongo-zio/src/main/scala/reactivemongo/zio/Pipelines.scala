package reactivemongo.zio

import reactivemongo.api.Collation
import reactivemongo.api.bson.collection.BSONSerializationPack.Writer
import reactivemongo.core.errors.ReactiveMongoException
import reactivemongo.zio.results.MultiBulkWriteResult

import zio.stream.ZPipeline
import zio.{ Chunk, Trace, ZIO }

object Pipelines {
  def insertAll[T: Writer](implicit
    trace: Trace
  ): ZPipeline[BZONCollection, ReactiveMongoException, T, MultiBulkWriteResult] =
    ZPipeline.mapChunksZIO { in =>
      Mongo.insert
        .many(in)
        .map(Chunk.single)
    }

  def updateAll[S: Writer, T: Writer](
    upsert: Boolean = false,
    multi: Boolean = false,
    collation: Option[Collation] = None
  )(implicit trace: Trace): ZPipeline[BZONCollection, ReactiveMongoException, (S, T), MultiBulkWriteResult] =
    ZPipeline.mapChunksZIO { in =>
      in.nonEmptyOrElse[ZIO[BZONCollection, ReactiveMongoException, Chunk[MultiBulkWriteResult]]](
        ZIO.succeed(Chunk.empty[MultiBulkWriteResult])
      ) {
        _.mapZIO { case (selector, update) =>
          Mongo.update.element(selector, update, upsert = upsert, multi = multi, collation = collation)
        }
          .flatMap(Mongo.update.many)
          .map(Chunk.single)
      }
    }
}
