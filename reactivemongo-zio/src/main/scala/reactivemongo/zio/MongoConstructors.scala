package reactivemongo.zio

import reactivemongo.api.DB

import zio.{ Config, Scope, Task, Trace, ZIO, ZLayer }

private[zio] trait MongoConstructors extends MongoConstructorsVersionSpecific {

  /**
   * Constructs a new Mongo instance using the underlying connection and the provided database name.
   *
   * @param databaseName The name of the database to use
   */
  def make(databaseName: => String)(implicit trace: Trace): ZIO[Connection, Throwable, Mongo] =
    ZIO.serviceWith[Connection] { connection =>
      new Mongo {
        def database(implicit trace: Trace): Task[DB] =
          connection.database(databaseName)
      }
    }

  /**
   * Constructs a new Mongo instance using the underlying connection. This method requires that a database name be specified in the connection
   * when it is created.
   */
  def make(implicit trace: Trace): ZIO[Connection, Throwable, Mongo] =
    ZIO.serviceWithZIO[Connection] { connection =>
      ZIO
        .getOrFailWith(new IllegalArgumentException("No database name specified"))(
          connection.uri.db
        )
        .flatMap(make(_))
    }

  /**
   * Constructs a new Mongo instance using the underlying connection, the database name is specified as part of the built-in configuration
   * which can be extract using the provided function
   */
  def makeConfig[C](config: Config[C])(f: C => String)(implicit trace: Trace): ZIO[Connection, Throwable, Mongo] =
    ZIO.config(config).flatMap(c => make(f(c)))

  /**
   * Returns a layer which constructs a new Mongo instance using the underlying connection. This method accepts a database name which will be used instead
   * of the dbName specified in the connection string (if any)
   *
   * @param dbName Name of the database to use
   */
  def layer(dbName: => String)(implicit trace: Trace): ZLayer[Connection, Throwable, Mongo] =
    ZLayer(make(dbName))

  /**
   * Returns a layer which constructs a new Mongo instance using the underlying connection. This method requires that a database name be specified in the connection
   * when it is created.
   */
  def layer(implicit trace: Trace): ZLayer[Connection, Throwable, Mongo] =
    ZLayer(make)

  /**
   * Returns a layer which constructs a new Mongo instance using the underlying connection, the database name is specified as part of the built-in configuration
   * which can be extract using the provided function
   */
  def layerConfig[C](config: Config[C])(f: C => String)(implicit trace: Trace): ZLayer[Connection, Throwable, Mongo] =
    ZLayer(makeConfig(config)(f))

  /**
   * Produces a new scoped effect which will return a `Mongo` instance given a connection string.
   *
   * @param uri A mongo connection uri starting with `mongodb://` or `mongodb+srv://`
   */
  def fromConnectionStringScoped(uri: String, poolName: Option[String] = None)(implicit
    trace: Trace
  ): ZIO[Scope, Throwable, Mongo] =
    make.provideSome[Scope](
      Connection.fromConnectionString(uri, poolName).extendScope,
      MongoDriver.layer.extendScope
    )

  /**
   * Constructs a new mongo layer given a uri connection string. When this layer is destructed it will
   * also handle tearing down the mongo connection
   *
   * @param uri The mongo connection string should start with `mongo://` or `mongo+srv://`
   * @return
   */
  def fromConnectionString(uri: String)(implicit trace: Trace): ZLayer[Any, Throwable, Mongo] =
    ZLayer.scoped(fromConnectionStringScoped(uri))

  /**
   * Loads the mongo configuration using the built in config system. It expects to be able to
   * retrieve a `mongo.uri` value which is represented as valid url connection string.
   *
   * @see [[fromEnvironmentConfigScoped]] if you need to extract the string from the environment
   */
  def fromConfigScoped(implicit trace: Trace): ZIO[Scope, Throwable, Mongo] =
    ZIO.config(MongoConfig.config).flatMap(c => fromConnectionStringScoped(c.uri))

  /**
   * Loads the mongo configuration using the built in config system. It expects to be able to
   * retrieve a `mongo.uri` value which is represented as valid url connection string.
   *
   * @see [[fromEnvironmentConfig]] if you need to extract the string from the environment
   */
  def fromConfig(implicit trace: Trace): ZLayer[Any, Throwable, Mongo] =
    ZLayer.scoped(fromConfigScoped)

}
