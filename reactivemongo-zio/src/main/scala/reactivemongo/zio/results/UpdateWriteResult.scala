package reactivemongo.zio.results

import reactivemongo.api.bson.collection.BSONCollection
import reactivemongo.api.commands.{ WriteConcernError, WriteError }

case class UpdateWriteResult(result: BSONCollection#UpdateWriteResult) extends AnyVal {
  def ok: Boolean                                  = result.code.isEmpty
  def n: Int                                       = result.n
  def nModified: Int                               = result.nModified
  def upserted: Seq[Upserted]                      = result.upserted.map(Upserted.apply)
  def writeErrors: Seq[WriteError]                 = result.writeErrors
  def writeConcernError: Option[WriteConcernError] = result.writeConcernError
  def code: Option[Int]                            = result.code
  def errmsg: Option[String]                       = result.errmsg
}
