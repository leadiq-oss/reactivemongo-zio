package reactivemongo.zio.results

import reactivemongo.api.bson.BSONValue
import reactivemongo.api.bson.collection.BSONCollection
import reactivemongo.api.commands.{ WriteConcernError, WriteError }

case class MultiBulkWriteResult(result: BSONCollection#MultiBulkWriteResult) extends AnyVal {
  @inline def ok: Boolean                                  = result.ok
  @inline def n: Int                                       = result.n
  @inline def nModified: Int                               = result.nModified
  @inline def upserted: Seq[Upserted]                      = result.upserted.map(Upserted.apply)
  @inline def writeErrors: Seq[WriteError]                 = result.writeErrors
  @inline def writeConcernError: Option[WriteConcernError] = result.writeConcernError
  @inline def code: Option[Int]                            = result.code
  @inline def errmsg: Option[String]                       = result.errmsg
  @inline def totalN: Int                                  = result.totalN

}

case class Upserted(upserted: BSONCollection#Upserted) extends AnyVal {
  def index: Int     = upserted.index
  def _id: BSONValue = upserted._id
}
