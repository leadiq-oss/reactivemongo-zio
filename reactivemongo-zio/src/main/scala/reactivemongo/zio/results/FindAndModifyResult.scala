package reactivemongo.zio.results

import reactivemongo.api.bson.collection.BSONCollection
import reactivemongo.api.bson.collection.BSONSerializationPack.Reader
import reactivemongo.api.bson.{ BSONDocument, BSONValue }

case class FindAndModifyResult(wrapped: BSONCollection#FindAndModifyResult) extends AnyVal {

  @inline def lastError: Option[FindAndUpdateLastError] = wrapped.lastError.map(FindAndUpdateLastError.apply)
  @inline def value: Option[BSONDocument]               = wrapped.value

  def result[T](implicit reader: Reader[T]): Option[T] =
    wrapped.result[T]
}

case class FindAndUpdateLastError(error: BSONCollection#FindAndUpdateLastError) {

  /** Indicates whether an existing document has been updated */
  @inline def updatedExisting: Boolean = error.updatedExisting

  /** The value of the upserted ID */
  @inline def upserted: Option[BSONValue] = error.upserted

  /** The number of updated document */
  @inline def n: Int = error.n

  /** The error message (if any) */
  @inline def err: Option[String] = error.err

}
