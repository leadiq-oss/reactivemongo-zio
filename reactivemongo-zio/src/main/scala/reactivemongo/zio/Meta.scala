package reactivemongo.zio

import reactivemongo.api.{ CollectionStats, ReadPreference }

import zio.{ Trace, ZIO }

trait Meta {
  def ping(readPreference: ReadPreference = ReadPreference.nearest)(implicit
    trace: Trace
  ): ZIO[Mongo, Throwable, Boolean] =
    Mongo.database.flatMap(db => ZIO.fromFuture(implicit ec => db.ping(readPreference)))

  def stats(implicit trace: Trace): ZIO[BZONCollection, Throwable, CollectionStats] =
    ZIO.serviceWithZIO[BZONCollection](coll => ZIO.fromFuture(implicit ec => coll.stats()))

  def stats(scale: Int)(implicit trace: Trace): ZIO[BZONCollection, Throwable, CollectionStats] =
    ZIO.serviceWithZIO[BZONCollection](coll => ZIO.fromFuture(implicit ec => coll.stats(scale)))
}
