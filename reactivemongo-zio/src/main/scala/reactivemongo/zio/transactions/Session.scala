package reactivemongo.zio.transactions

import reactivemongo.zio.Mongo
import reactivemongo.zio.MongoOverrideContext.writeConcernWith

import zio._

/**
 * A session is the logical equivalent to a mongo session
 */
sealed trait Session {

  /**
   * Runs an effect (and any associated mongo queries inside it) using a Mongo Transaction.
   * The resulting transaction will be invisible until the effect returned by this method completes, and if it
   * fails or is interrupted the transaction will be safely aborted.
   */
  def transaction[R, E, A](effect: => ZIO[R & Mongo & Session, E, A])(implicit tag: Tag[Mongo]): ZIO[R, E, A]
}

object Session {

  /**
   * For nested transactions we want them to adopt the outer boundaries
   * of their parent transaction, this is because we are only allowed to have a single
   * transaction in flight for a session, and using a lock would cause a deadlock.
   *
   * e.g.
   * transaction { // A
   *   transaction { // B
   *     ...
   *   }
   * }
   *
   * B will adopt the bounds of A, and will be able to use the same session.
   */
  private class Inner(outer: Session, mongo: Mongo) extends Session {
    def transaction[R, E, A](effect: => ZIO[R & Mongo & Session, E, A])(implicit tag: Tag[Mongo]): ZIO[R, E, A] =
      effect.provideSomeEnvironment[R](_.add[Mongo](mongo).add[Session](outer))
  }

  def makeTransaction[R, E, A](effect: => ZIO[R & Mongo & Session, E, A])(implicit
    tag: Tag[Mongo]
  ): ZIO[R & Session, E, A] =
    ZIO.serviceWithZIO[Session](_.transaction[R, E, A](effect))

  def make: ZIO[Scope & Mongo, Throwable, Session] = for {
    s    <- Semaphore.make(1)
    m    <- ZIO.service[Mongo]
    db   <- m.database.flatMap { outer =>
              ZIO.fromFuture(implicit ec => outer.startSession(failIfAlreadyStarted = true)).withFinalizerExit {
                (db, exit) =>
                  exit
                    .foldZIO(
                      _ => ZIO.fromFuture(implicit ec => db.killSession(true)),
                      _ => ZIO.fromFuture(implicit ec => db.endSession(true))
                    )
                    .orDie
              }
            }
    proxy = Mongo.Proxy(m, db)
  } yield new Session { self =>
    def transaction[R, E, A](effect: => ZIO[R & Mongo & Session, E, A])(implicit tag: Tag[Mongo]): ZIO[R, E, A] =
      ZIO.scoped[R](
        writeConcernWith(wc =>
          for {
            _      <-
              s.withPermit(
                ZIO
                  .fromFuture(implicit ec => db.startTransaction(wc, true))
                  .withFinalizerExit { (db, exit) =>
                    exit
                      .foldZIO(
                        _ => ZIO.fromFuture(implicit ec => db.abortTransaction(true)),
                        _ => ZIO.fromFuture(implicit ec => db.commitTransaction(true))
                      )
                      .orDie
                  }
                  .orDie
              )
            result <- effect.provideSomeEnvironment[R](_.add[Mongo](proxy).add[Session](new Inner(self, m)))
          } yield result
        )
      )

  }
}
