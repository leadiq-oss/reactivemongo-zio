package reactivemongo.zio.transactions

import reactivemongo.zio.Mongo
import reactivemongo.zio.transactions.Transactions.{ SessionPartiallyAppliedMongo, TransactionPartiallyAppliedMongo }
import zio._

trait Transactions {

  /**
   * Runs an effect that requires a mongo in the context of a session.
   */
  def session[R]: SessionPartiallyAppliedMongo[R] =
    new SessionPartiallyAppliedMongo[R]()

  /**
   * Runs the provided result within the context of a mongo transaction, which allows multiple queries
   * to be executed atomically.
   */
  def transaction[R]: TransactionPartiallyAppliedMongo[R] =
    new TransactionPartiallyAppliedMongo[R]()
}

object Transactions {
  class TransactionPartiallyAppliedMongo[R](private val dummy: Boolean = true) extends AnyVal {
    def apply[E, A](
      effect: => ZIO[Mongo & Session & R, E, A]
    )(implicit trace: Trace): ZIO[R & Session & Mongo, E, A] =
      ZIO.serviceWithZIO[Mongo](_.transaction[R](effect))
  }

  class SessionPartiallyAppliedMongo[R](private val dummy: Boolean = true) extends AnyVal {
    def apply[E, A](
      effect: => ZIO[Session & R, E, A]
    )(implicit trace: Trace, tag: Tag[Session]): ZIO[R & Mongo, E, A] =
      ZIO.serviceWithZIO[Mongo](_.session[R](effect))
  }
}
