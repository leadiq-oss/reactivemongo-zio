package reactivemongo.zio

import scala.language.implicitConversions

import zio._
import zio.stream.ZStream

trait MongoSyntax {
  implicit def toMongoSyntax[R, E, A](
    query: ZIO[BZONCollection & R, E, A]
  ): MongoSyntax.CollectionZIOExtensions[R, E, A] =
    new MongoSyntax.CollectionZIOExtensions[R, E, A](query)

  implicit def toMongoStreamSyntax[R, E, A](
    stream: ZStream[BZONCollection & R, E, A]
  ): MongoSyntax.CollectionStreamExtensions[R, E, A] =
    new MongoSyntax.CollectionStreamExtensions[R, E, A](stream)
}

object MongoSyntax {
  class CollectionZIOExtensions[R, E, A](val query: ZIO[BZONCollection & R, E, A]) extends AnyVal {
    def collection(name: String): ZIO[Mongo & R, E, A] =
      collection(CollectionName(name))

    def collection(name: CollectionName): ZIO[Mongo & R, E, A] =
      Mongo.collectionWithZIO[R, E, A](name)(query)
  }

  class CollectionStreamExtensions[R, E, A](val stream: ZStream[BZONCollection & R, E, A]) extends AnyVal {
    def collection(name: String): ZStream[Mongo & R, E, A] =
      collection(CollectionName(name))

    def collection(name: CollectionName): ZStream[Mongo & R, E, A] =
      Mongo.collectionWithStream[R, E, A](name)(stream)
  }
}
