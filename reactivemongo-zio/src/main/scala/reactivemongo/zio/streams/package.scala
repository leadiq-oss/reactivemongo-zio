package reactivemongo.zio

import reactivemongo.api.{ Cursor, CursorProducer }
import reactivemongo.zio.ZCursor.WithOps

import zio.Trace

package object streams {

  /** Provides ZIO Cursor instances for CursorProducer typeclass. */
  implicit def cursorProducer[T](implicit trace: Trace): CursorProducer[T] { type ProducedCursor = WithOps[T] } =
    new CursorProducer[T] {
      type ProducedCursor = ZCursor.WithOps[T]

      // Returns a cursor with ZIO Streams operations.
      def produce(c: Cursor.WithOps[T]): ProducedCursor =
        ZCursorImpl(c)
    }
}
