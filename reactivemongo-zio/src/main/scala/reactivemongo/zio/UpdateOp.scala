package reactivemongo.zio

import scala.annotation.nowarn

import reactivemongo.api.Collation
import reactivemongo.api.bson.BSONDocument
import reactivemongo.api.bson.collection.BSONSerializationPack
import reactivemongo.api.bson.collection.BSONSerializationPack.Writer
import reactivemongo.core.errors.ReactiveMongoException
import reactivemongo.zio.MongoOverrideContext.writeConcernWith
import reactivemongo.zio.aggregation.ZAggregationFramework
import reactivemongo.zio.results.{ MultiBulkWriteResult, UpdateWriteResult }

import zio._

final case class UpdateOp private[zio] (ordered: Boolean, bypassDocumentValidation: Boolean) {

  private val builder: URIO[BZONCollection, BZONCollection#UpdateBuilder] =
    writeConcernWith(wc =>
      ZIO.serviceWith[BZONCollection](b =>
        wc.fold(b.update(ordered, bypassDocumentValidation))(b.update(ordered, _, bypassDocumentValidation))
      )
    )

  def one[Q, U](
    q: Q,
    u: U,
    upsert: Boolean = false,
    multi: Boolean = false
  )(implicit
    qw: Writer[Q],
    uw: Writer[U],
    trace: Trace
  ): ZIO[BZONCollection, ReactiveMongoException, UpdateWriteResult]                                           =
    builder
      .flatMap(builder =>
        ZIO
          .fromFuture(implicit ec => builder.one[Q, U](q, u, upsert = upsert, multi = multi)(ec, qw, uw))
      )
      .map(UpdateWriteResult.apply)
      .refineOrDie { case e: ReactiveMongoException => e }

  @nowarn("msg=deprecated")
  def one[Q](
    q: Q,
    pipeline: ZAggregationFramework.Pipeline,
    upsert: Boolean,
    multi: Boolean,
    collation: Option[Collation],
    arrayFilters: Seq[BSONDocument]
  )(implicit writer: Writer[Q], trace: Trace): ZIO[BZONCollection, ReactiveMongoException, UpdateWriteResult] =
    ZIO.serviceWithZIO[BZONCollection] { coll =>
      builder
        .flatMap(builder =>
          element(q, pipeline, upsert, multi, collation, arrayFilters).flatMap { el =>
            ZIO.fromFuture(implicit ec =>
              // FIXME this is a hack, because the type system can't figure out that the coll type matches that of builder
              coll
                .update(builder.ordered, builder.writeConcern, builder.bypassDocumentValidation)
                .one(el.toElement(coll))
            )
          }
        )
        .map(UpdateWriteResult.apply)
        .refineOrDie { case e: ReactiveMongoException => e }
    }

  def element[Q](
    q: Q,
    u: ZAggregationFramework.Pipeline,
    upsert: Boolean,
    multi: Boolean,
    collation: Option[Collation],
    arrayFilters: Seq[BSONDocument]
  )(implicit writer: Writer[Q]): ZIO[Any, Throwable, UpdateElement] =
    ZIO.attempt(BSONSerializationPack.serialize(q, writer)).map { query =>
      new UpdateElement(query, Right(u.map(_.makePipe)), upsert, multi, collation, arrayFilters)
    }

  /** Prepares an [[UpdateElement]] */
  def element[Q: Writer, U: Writer](q: Q, u: U, upsert: Boolean = false, multi: Boolean = false)(implicit
    trace: Trace
  ): ZIO[BZONCollection, ReactiveMongoException, UpdateElement] =
    element(q, u, upsert, multi, None, Seq.empty)

  /** Prepares an [[UpdateElement]] */
  def element[Q: Writer, U: Writer](q: Q, u: U, upsert: Boolean, multi: Boolean, collation: Option[Collation])(implicit
    trace: Trace
  ): ZIO[BZONCollection, ReactiveMongoException, UpdateElement] =
    element(q, u, upsert, multi, collation, Seq.empty)

  /** Prepares an [[UpdateElement]] */
  def element[Q, U](
    q: Q,
    u: U,
    upsert: Boolean,
    multi: Boolean,
    collation: Option[Collation],
    arrayFilters: Seq[BSONDocument]
  )(implicit
    qw: Writer[Q],
    uw: Writer[U],
    trace: Trace
  ): ZIO[Any, ReactiveMongoException, UpdateElement]                                                  =
    ZIO
      .attempt(BSONSerializationPack.serialize(q, qw))
      .map(
        new UpdateElement(
          _,
          Left(BSONSerializationPack.serialize(u, uw)),
          upsert,
          multi,
          collation,
          arrayFilters
        )
      )
      .refineOrDie { case e: ReactiveMongoException => e }

  /**
   * [[https://docs.mongodb.com/manual/reference/method/db.collection.updateMany/ Updates many documents]], according the ordered behaviour.
   *
   * {{{
   * import reactivemongo.api.bson.BSONDocument
   * import reactivemongo.api.bson.collection.BZONCollection
   *
   * def updateMany(
   *   coll: BZONCollection,
   *   docs: NonEmptyChunk[BSONDocument]) = {
   *   val update = coll.update(ordered = true).flatMap { update =>
   *
   *     val elements = ZIO.collectAll(docs.map { doc =>
   *       update.element(
   *         q = BSONDocument("update" -> "selector"),
   *         u = BSONDocument(f"$$set" -> doc),
   *         upsert = true,
   *         multi = false)
   *     })
   *
   *     elements.flatMap { ups =>
   *       update.many(ups) // Future[MultiBulkWriteResult]
   *     }
   *   }
   * }
   * }}}
   */
  def many(
    updates: NonEmptyChunk[UpdateElement]
  )(implicit trace: Trace): ZIO[BZONCollection, ReactiveMongoException, results.MultiBulkWriteResult] =
    writeConcernWith(w =>
      ZIO.serviceWithZIO[BZONCollection] { c =>
        ZIO
          .fromFuture(implicit ec =>
            w.fold(c.update(ordered, bypassDocumentValidation))(c.update(ordered, _, bypassDocumentValidation))
              .many(updates.head.toElement(c), updates.tail.map(_.toElement(c)))
              .map(MultiBulkWriteResult(_))
          )
          .refineOrDie { case e: ReactiveMongoException =>
            e
          }
      }
    )

  /**
   * [[https://docs.mongodb.com/manual/reference/method/db.collection.updateMany/ Updates many documents]], according the ordered behaviour.
   *
   * {{{
   * import reactivemongo.api.bson.BSONDocument
   * import reactivemongo.api.bson.collection.BZONCollection
   *
   * def updateMany(
   *   coll: BZONCollection,
   *   docs: NonEmptyChunk[BSONDocument]) = {
   *   val update = coll.update(ordered = true).flatMap { update =>
   *
   *     update.collectMany(docs.map { doc =>
   *       update.element(
   *         q = BSONDocument("update" -> "selector"),
   *         u = BSONDocument(f"$$set" -> doc),
   *         upsert = true,
   *         multi = false)
   *     })
   *   }
   * }
   * }}}
   */
  def collectMany[R](
    updates: NonEmptyChunk[ZIO[R, ReactiveMongoException, UpdateElement]]
  )(implicit trace: Trace): ZIO[R & BZONCollection, ReactiveMongoException, results.MultiBulkWriteResult] =
    ZIO.collectAll(updates).flatMap(many)

}

final class UpdateElement(
  val q: BSONDocument,
  val u: Either[BSONDocument, Seq[BSONDocument]],
  val upsert: Boolean,
  val multi: Boolean,
  val collation: Option[Collation],
  val arrayFilters: Seq[BSONDocument]
) {

  private[zio] def toElement(coll: BZONCollection): coll.UpdateElement =
    new coll.UpdateElement(q, u, upsert, multi, collation, arrayFilters)
}
