package reactivemongo

import reactivemongo.api._

package object zio extends MongoSyntax {

  type MongoDriver    = AsyncDriver
  type ZPack          = reactivemongo.api.bson.collection.BSONSerializationPack.type
  type BZONCollection = reactivemongo.api.collections.GenericCollection[ZPack]
}
