package reactivemongo.zio

import scala.concurrent.duration.{ FiniteDuration, SECONDS }
import reactivemongo.api.MongoConnection.ParsedURI
import reactivemongo.api.{ DB, MongoConnection }
import reactivemongo.zio.MongoOverrideContext.failoverStrategyWith
import zio._

trait Connection {
  def uri: ParsedURI

  def database(name: String)(implicit trace: Trace): ZIO[Any, Throwable, DB]
}

object Connection {

  def fromConnectionStringScoped(
    uri: => String,
    poolName: Option[String] = None
  )(implicit trace: Trace): ZIO[Scope & MongoDriver, Throwable, Connection] =
    for {
      driver    <- ZIO.service[MongoDriver]
      parsedURI <- ZIO.blocking(ZIO.fromFuture(implicit ec => MongoConnection.fromString(uri))) <* ZIO.yieldNow
      conn      <- ZIO
                     .fromFuture(implicit ec => driver.connect(parsedURI, poolName))
                     .withFinalizer(c =>
                       ZIO.fromFuture(implicit ec => c.close()(FiniteDuration(10, SECONDS))).timeout(12.seconds).orDie
                     )
    } yield new Connection {
      def database(name: String)(implicit trace: Trace): ZIO[Any, Throwable, DB] =
        failoverStrategyWith(fs => ZIO.fromFuture(implicit ec => fs.fold(conn.database(name))(conn.database(name, _))))

      def uri: ParsedURI = parsedURI
    }

  def fromConnectionString(uri: => String, name: Option[String] = None)(implicit
    trace: Trace
  ): ZLayer[MongoDriver, Throwable, Connection] =
    ZLayer.scoped[MongoDriver](fromConnectionStringScoped(uri, name))

  def fromEnvironmentConfigScoped[C](
    f: C => String,
    name: Option[String] = None
  )(implicit tagged: Tag[C], trace: Trace): ZIO[Scope & MongoDriver & C, Throwable, Connection] =
    ZIO.serviceWithZIO[C](c => fromConnectionStringScoped(f(c), name))

  def fromEnvironmentConfig[C](
    f: C => String,
    name: Option[String] = None
  )(implicit tagged: Tag[C], trace: Trace): ZLayer[MongoDriver & C, Throwable, Connection] =
    ZLayer.scoped[MongoDriver & C](ZIO.service[C].flatMap(c => fromConnectionStringScoped(f(c), name)))

  def fromConfigScoped(implicit trace: Trace): ZIO[Scope & MongoDriver, Throwable, Connection] =
    ZIO.config(MongoConfig.config).flatMap(c => fromConnectionStringScoped(c.uri, c.name))

  def fromConfigScoped[C](
    config: Config[C]
  )(f: C => MongoConfig)(implicit trace: Trace): ZIO[Scope & MongoDriver, Throwable, Connection] =
    ZIO.config(config).flatMap { c =>
      val mongoConfig = f(c)
      fromConnectionStringScoped(mongoConfig.uri, mongoConfig.name)
    }

  def fromConfig(implicit trace: Trace): ZLayer[MongoDriver, Throwable, Connection] =
    ZLayer.scoped[MongoDriver](fromConfigScoped)

  def fromConfig[C](
    config: Config[C]
  )(f: C => MongoConfig)(implicit trace: Trace): ZLayer[MongoDriver, Throwable, Connection] =
    ZLayer.scoped[MongoDriver](fromConfigScoped(config)(f))

}
