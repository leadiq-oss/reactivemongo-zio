package reactivemongo.zio

import reactivemongo.api.{ FailoverStrategy, ReadConcern, ReadPreference, WriteConcern }

import zio.stream.{ ZStream, ZStreamAspect }
import zio.{ Trace, ZIO, ZIOAspect }

object MongoAspect {

  def usingContext(
    f: MongoOverrideContext => MongoOverrideContext
  ): ZIOAspect[Nothing, Any, Nothing, Any, Nothing, Any] =
    new ZIOAspect[Nothing, Any, Nothing, Any, Nothing, Any] {
      def apply[R, E, A](zio: ZIO[R, E, A])(implicit trace: Trace): ZIO[R, E, A] =
        MongoOverrideContext.overrideMongoContext.locallyWith(f)(zio)
    }

  def readPreference(preference: ReadPreference): ZIOAspect[Nothing, Any, Nothing, Any, Nothing, Any] =
    usingContext(_.copy(readPreference = Some(preference)))

  def readConcern(concern: ReadConcern): ZIOAspect[Nothing, Any, Nothing, Any, Nothing, Any] =
    usingContext(_.copy(readConcern = Some(concern)))

  def writeConcern(concern: WriteConcern): ZIOAspect[Nothing, Any, Nothing, Any, Nothing, Any] =
    usingContext(_.copy(writeConcern = Some(concern)))

  def failoverStrategy(strategy: => FailoverStrategy): ZIOAspect[Nothing, Any, Nothing, Any, Nothing, Any] =
    usingContext(_.copy(failoverStrategy = Some(strategy)))

}

object MongoStreamAspect {

  def usingContext(
    f: MongoOverrideContext => MongoOverrideContext
  ): ZStreamAspect[Nothing, Any, Nothing, Any, Nothing, Any] =
    new ZStreamAspect[Nothing, Any, Nothing, Any, Nothing, Any] {
      def apply[R, E, A](stream: ZStream[R, E, A])(implicit trace: Trace): ZStream[R, E, A] =
        ZStream.unwrapScoped(MongoOverrideContext.overrideMongoContext.locallyScopedWith(f) as stream)
    }

  def readPreference(preference: ReadPreference): ZStreamAspect[Nothing, Any, Nothing, Any, Nothing, Any] =
    usingContext(_.copy(readPreference = Some(preference)))

  def readConcern(concern: ReadConcern): ZStreamAspect[Nothing, Any, Nothing, Any, Nothing, Any] =
    usingContext(_.copy(readConcern = Some(concern)))

  def writeConcern(concern: WriteConcern): ZStreamAspect[Nothing, Any, Nothing, Any, Nothing, Any] =
    usingContext(_.copy(writeConcern = Some(concern)))

  def failoverStrategy(strategy: => FailoverStrategy): ZStreamAspect[Nothing, Any, Nothing, Any, Nothing, Any] =
    usingContext(_.copy(failoverStrategy = Some(strategy)))
}
