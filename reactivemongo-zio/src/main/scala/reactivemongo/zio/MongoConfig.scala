package reactivemongo.zio

import zio.Config

case class MongoConfig(
  uri: String,
  name: Option[String]
)

object MongoConfig {
  val config: Config[MongoConfig] =
    (Config.string("uri") zipWith Config.string("name").optional)(MongoConfig(_, _)).nested("mongo")
}
