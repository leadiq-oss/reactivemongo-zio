package reactivemongo.zio

import reactivemongo.api.{ Collection, CollectionProducer }

import zio.stream.ZStream
import zio._

trait Collections {

  /**
   * Returns a wrapper for a collection with the given name. Depending on the settings of the mongo cluster
   * using this collection may result in the collection being created if it doesn't yet exist
   * @param name The name of the collection
   */
  def getCollection[C <: Collection: CollectionProducer](
    name: String
  )(implicit trace: Trace): ZIO[Mongo, Throwable, C] =
    ZIO.serviceWithZIO(_.getCollection(name))

  def collection(collectionName: CollectionName)(implicit trace: Trace): ZIO[Mongo, Nothing, ZCollection] =
    ZIO.serviceWith(_.collection(collectionName))

  def collectionNames(implicit trace: Trace): ZIO[Mongo, Throwable, List[String]] =
    Mongo.database.flatMap(db => ZIO.fromFuture(implicit ec => db.collectionNames))

  def collectionWithZIO[R, E, A](name: CollectionName)(
    query: => ZIO[BZONCollection & R, E, A]
  )(implicit trace: Trace): ZIO[Mongo & R, E, A] =
    ZIO.serviceWithZIO[Mongo](_.collection(name).apply[R, E, A](query))

  def collectionWithZIO[R, E, A](name: String)(
    query: => ZIO[BZONCollection & R, E, A]
  )(implicit trace: Trace): ZIO[Mongo & R, E, A] =
    collectionWithZIO[R, E, A](CollectionName(name))(query)

  def collectionWithStream[R, E, A](name: CollectionName)(
    stream: => ZStream[BZONCollection & R, E, A]
  )(implicit trace: Trace): ZStream[Mongo & R, E, A] =
    ZStream.serviceWithStream[Mongo](_.collection(name).apply[R, E, A](stream))

  def collectionWithStream[R, E, A](name: String)(
    stream: => ZStream[BZONCollection & R, E, A]
  )(implicit trace: Trace): ZStream[Mongo & R, E, A] =
    collectionWithStream[R, E, A](CollectionName(name))(stream)

  def create(failsIfExists: Boolean = false)(implicit trace: Trace): ZIO[BZONCollection, Throwable, Unit] =
    ZIO.serviceWithZIO[BZONCollection](coll => ZIO.fromFuture(implicit ec => coll.create(failsIfExists)))

  def createCapped(size: Long, maxDocuments: Option[Int]): ZIO[BZONCollection, Throwable, Unit] =
    ZIO.serviceWithZIO[BZONCollection](coll => ZIO.fromFuture(implicit ec => coll.createCapped(size, maxDocuments)))

  def drop()(implicit trace: Trace): ZIO[BZONCollection, Throwable, Unit] =
    ZIO.serviceWithZIO[BZONCollection](coll => ZIO.fromFuture(implicit ec => coll.drop()))
}
