package reactivemongo.zio

import reactivemongo.api.{ FailoverStrategy, ReadConcern, ReadPreference, WriteConcern }

import zio.{ FiberRef, Trace, Unsafe, ZIO }

case class MongoOverrideContext(
  readPreference: Option[ReadPreference] = None,
  readConcern: Option[ReadConcern] = None,
  writeConcern: Option[WriteConcern] = None,
  failoverStrategy: Option[FailoverStrategy] = None
)

object MongoOverrideContext {

  private[zio] val overrideMongoContext: FiberRef[MongoOverrideContext] =
    Unsafe.unsafe(implicit u => FiberRef.unsafe.make[MongoOverrideContext](MongoOverrideContext()))

  def mongoContextWith[R, E, A](f: MongoOverrideContext => ZIO[R, E, A])(implicit trace: Trace): ZIO[R, E, A] =
    overrideMongoContext.getWith(f)

  def readPreferenceWith[R, E, A](f: Option[ReadPreference] => ZIO[R, E, A])(implicit trace: Trace): ZIO[R, E, A] =
    mongoContextWith(ctx => f(ctx.readPreference))

  def readConcernWith[R, E, A](f: Option[ReadConcern] => ZIO[R, E, A])(implicit trace: Trace): ZIO[R, E, A] =
    mongoContextWith(ctx => f(ctx.readConcern))

  def writeConcernWith[R, E, A](f: Option[WriteConcern] => ZIO[R, E, A])(implicit trace: Trace): ZIO[R, E, A] =
    mongoContextWith(ctx => f(ctx.writeConcern))

  def failoverStrategyWith[R, E, A](f: Option[FailoverStrategy] => ZIO[R, E, A])(implicit trace: Trace): ZIO[R, E, A] =
    mongoContextWith(ctx => f(ctx.failoverStrategy))

}
