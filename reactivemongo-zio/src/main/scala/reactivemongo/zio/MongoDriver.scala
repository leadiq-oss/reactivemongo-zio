package reactivemongo.zio

import java.util.concurrent.TimeUnit

import scala.concurrent.duration.FiniteDuration

import org.apache.pekko.actor.ActorSystem
import reactivemongo.api.AsyncDriver

import zio.{ Scope, Trace, ZIO, ZLayer, durationInt }

object MongoDriver {
  def make(implicit trace: Trace): ZIO[Scope, Nothing, MongoDriver] =
    ZIO
      .succeed(AsyncDriver())
      .withFinalizer(d =>
        ZIO.fromFuture(implicit ec => d.close(FiniteDuration(10, TimeUnit.SECONDS))).timeout(10.seconds).orDie
      )

  def layer(implicit trace: Trace): ZLayer[Any, Nothing, MongoDriver] = ZLayer.scoped(make)

  def driverSystem(implicit trace: Trace): ZIO[MongoDriver, Nothing, ActorSystem] =
    ZIO.serviceWith[MongoDriver](_.system)

}
