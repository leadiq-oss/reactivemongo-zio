package reactivemongo.zio

import reactivemongo.api.bson.collection.BSONSerializationPack.Reader
import reactivemongo.zio.MongoOverrideContext.readPreferenceWith
import reactivemongo.zio.ZCursor.ZErrorHandler

import zio.stream.ZStream
import zio.{ RIO, Trace, URIO, ZIO }

case class ZQueryBuilder(
  private val builder: URIO[BZONCollection, BZONCollection#QueryBuilder]
) {

  def one[T: Reader](implicit trace: Trace): RIO[BZONCollection, Option[T]] =
    readPreferenceWith(rp => builder.flatMap(b => ZIO.fromFuture(implicit ec => rp.fold(b.one[T])(b.one[T]))))

  def requireOne[T: Reader](implicit trace: Trace): RIO[BZONCollection, T] =
    readPreferenceWith(rp =>
      builder.flatMap(b => ZIO.fromFuture(implicit ec => rp.fold(b.requireOne[T])(b.requireOne[T])))
    )

  /**
   * Produces a pull based stream which will load up to a maximum number of elements from an underlying cursor.
   */
  def stream[T: Reader](
    maxDocs: Int = Int.MaxValue,
    err: ZErrorHandler[Option[T]] = ZCursor.FailOnError[Option[T]]()
  )(implicit trace: Trace): ZStream[BZONCollection, Throwable, T] = {
    import streams.cursorProducer
    ZStream.unwrap(
      readPreferenceWith(rp => builder.map(b => rp.fold(b.cursor[T]())(b.cursor[T](_)).documentStream(maxDocs, err)))
    )
  }

}
