package reactivemongo.zio

import scala.annotation.tailrec
import scala.util.{ Failure, Success, Try }

import reactivemongo.api.Cursor.{ Cont, Done, Fail, State }
import reactivemongo.api.{ Cursor, CursorOps, WrappedCursor, WrappedCursorOps }
import reactivemongo.core.errors.ReactiveMongoException
import reactivemongo.core.protocol.{ ReplyDocumentIteratorExhaustedException, Response }
import reactivemongo.zio.ZCursor.{ WithOps, ZErrorHandler }

import zio.stream.{ ZChannel, ZStream }
import zio.{ Chunk, _ }

sealed trait ZCursor[T] extends Cursor[T] {

  /**
   * Constructs a cursor stream that can be used to step through documents returned from the DB.
   * The implementation is pull-based and internally supports a chunk based encoding.
   */
  def documentStream(
    maxDocs: Int = Int.MaxValue,
    err: ZErrorHandler[Option[T]] = ZCursor.FailOnError()
  )(implicit trace: Trace): ZStream[Any, Throwable, T]

}

object ZCursor {
  type WithOps[T]       = ZCursor[T] & CursorOps[T]
  type ZErrorHandler[T] = (T, Throwable) => ZIO[Any, Nothing, Cursor.State[T]]

  def FailOnError[T](callback: (T, Throwable) => UIO[Unit] = (_: T, _: Throwable) => ZIO.unit): ZErrorHandler[T] =
    (v: T, e: Throwable) => { callback(v, e) as (Fail(e): State[T]) }

  def ContOnError[T](callback: (T, Throwable) => UIO[Unit] = (_: T, _: Throwable) => ZIO.unit): ZErrorHandler[T] =
    (v: T, e: Throwable) => { callback(v, e) as (Cont(v): State[T]) }

  def DoneOnError[T](callback: (T, Throwable) => UIO[Unit] = (_: T, _: Throwable) => ZIO.unit): ZErrorHandler[T] =
    (v: T, e: Throwable) => { callback(v, e) as (Done(v): State[T]) }
}

final case class ZCursorImpl[T](protected val wrappee: Cursor.WithOps[T])
    extends WrappedCursor[T]
    with WrappedCursorOps[T]
    with ZCursor[T] {
  self =>

  @inline def opsWrappee = wrappee

  def documentStream(
    maxDocs: Int = Int.MaxValue,
    err: ZErrorHandler[Option[T]] = ZCursor.FailOnError()
  )(implicit trace: Trace): ZStream[Any, Throwable, T] =
    ZStream.unwrapScoped(for {
      ref      <- Ref.make(Option.empty[(Response, Option[T])])
      unroller <- ResponseUnroller.make(self, maxDocs, ref, err)
      cursor   <- CursorDriver.make(ref, unroller.unroll, killCursorZIO)
      puller   <- ResponsePuller.make(tailable, new RequestMaker(self, maxDocs, cursor))
      loop      = DriverLoop(wrappee, cursor, puller, err)
    } yield loop.stream)

  private def killCursorZIO(r: Response)(implicit trace: Trace): ZIO[Any, Throwable, Unit] =
    ZIO.executorWith { executor =>
      val ec = executor.asExecutionContext
      ZIO.attempt(wrappee.killCursor(r.reply.cursorID)(ec))
    }

}

private[zio] final case class DriverLoop[T](
  wrappee: Cursor.WithOps[T],
  cursor: CursorDriver[T],
  puller: ResponsePuller,
  errorHandler: ZErrorHandler[Option[T]]
) {
  def stream(implicit trace: Trace): ZStream[Any, ReactiveMongoException, T] = ZStream.suspend {
    ZStream
      .fromChannel(puller.pullNextResponse >>> onPull >>> cursor.pullDocuments)
      .refineOrDie { case e: ReactiveMongoException => e }
  }

  private def onPull(implicit trace: Trace): ZChannel[Any, Throwable, Response, Any, Throwable, Response, Unit] =
    ZChannel.readWithCause(
      onPullSucceed,
      onPullFailure,
      (_: Any) => onPullDone
    )

  private def onPullSucceed(response: Response)(implicit trace: Trace) =
    if (response.reply.numberReturned == 0) {
      if (wrappee.tailable) onPull
      else ZChannel.unit
    } else ZChannel.write(response) *> onPull

  private def onPullFailure(cause: Cause[Throwable])(implicit trace: Trace) =
    ZChannel.unwrap(
      (cursor.last.unsome.map(_.flatMap(_._2)) <* cursor.killAndReset).map(previous =>
        cause.failureOrCause match {
          case Left(reason) => handleError(previous, reason)
          case Right(c)     => ZChannel.failCause(c)
        }
      )
    )

  private def handleError(
    previous: Option[T],
    reason: Throwable
  ): ZChannel[Any, Any, Any, Any, Throwable, Nothing, Unit] =
    ZChannel.unwrap(errorHandler(previous, reason) map {
      case Cursor.Cont(_)     =>
        ZChannel.unit // TODO - This is a lie, we can't continue because we have killed the cursor but the "continue" strategy is deceptive
      case Cursor.Fail(error) => ZChannel.fail(error)
      case Cursor.Done(_)     => ZChannel.unit
      case x                  =>
        ZChannel.failCause(
          Cause.die(
            new IllegalArgumentException(
              s"$x: Should never come here, but 'match may not be exhaustive'"
            )
          )
        )
    })

  private def onPullDone(implicit trace: Trace) =
    if (!wrappee.tailable) ZChannel.unit
    else ZChannel.fromZIO(cursor.reset) *> onPull // Impossible case: tailable cursors can't return `None`
}

private final class RequestMaker[T](
  wrappee: WithOps[T],
  maxDocs: Int,
  driver: CursorDriver[T]
) {
  private val nextResponse = wrappee.nextResponse(maxDocs)

  def requestNext(implicit trace: Trace): ZIO[Any, Option[Throwable], Response] =
    for {
      lastResponse <- driver.last.map(_._1)
      newResponse  <- nextR(lastResponse).some
      _            <- driver.killAndReset.when(newResponse.reply.cursorID != lastResponse.reply.cursorID)
    } yield newResponse

  def requestInitial(implicit trace: Trace): Task[Response] =
    ZIO.fromFuture(wrappee.makeRequest(maxDocs)(_))

  private def nextR(r: Response)(implicit trace: Trace) =
    ZIO.fromFuture(nextResponse(_, r))
}

private[zio] object ResponsePuller {
  def make[T](
    tailable: Boolean,
    requestMaker: RequestMaker[T]
  )(implicit
    trace: Trace
  ): UIO[ResponsePuller] =
    ZIO.succeed(
      if (tailable) new TailableResponsePuller(requestMaker)
      else new DefaultResponsePuller(requestMaker)
    )
}

private[zio] trait ResponsePuller {
  def pullNextResponse(implicit trace: Trace): ZChannel[Any, Any, Any, Any, Throwable, Response, Unit]

}

private[zio] final class TailableResponsePuller[T](requestMaker: RequestMaker[T]) extends ResponsePuller {
  def pullNextResponse(implicit trace: Trace): ZChannel[Any, Any, Any, Any, Throwable, Response, Unit] =
    ZChannel.unwrap(requestMaker.requestInitial.map(ZChannel.write)) *> pullNextResponse
}

private[zio] final class DefaultResponsePuller[T](
  requestMaker: RequestMaker[T]
) extends ResponsePuller {

  def pullNextResponse(implicit trace: Trace): ZChannel[Any, Any, Any, Any, Throwable, Response, Unit] =
    getMore(first = false)

  private def getMore(
    first: Boolean
  )(implicit trace: Trace): ZChannel[Any, Any, Any, Any, Throwable, Response, Unit] =
    ZChannel.suspend {
      if (!first)
        ZChannel.unwrap(requestMaker.requestInitial.map(r => ZChannel.write(r) *> getMore(r.reply.numberReturned > 0)))
      else
        ZChannel.unwrap(requestMaker.requestNext.unsome.map {
          case Some(r) => ZChannel.write(r) *> getMore(first = true)
          case None    => ZChannel.unit
        })
    }
}

private[zio] final class ResponseUnroller[T](
  wrappee: WithOps[T],
  maxDocs: Int,
  state: Ref[Option[(Response, Option[T])]],
  errorHandler: ZErrorHandler[Option[T]]
) {

  def unroll(response: Response)(implicit
    trace: Trace
  ): ZChannel[Any, Any, Any, Any, Throwable, Chunk[T], Unit] =
    fromResponse(response).flatMap { it =>
      def reader(last: Option[T]): ZChannel[Any, Any, Any, Any, Throwable, Chunk[T], Unit] = ZChannel.suspend(
        if (it.isEmpty) ZChannel.unit
        else go(Chunk.newBuilder[T], last)
      )

      @tailrec
      def go(buffer: ChunkBuilder[T], lastValue: Option[T]): ZChannel[Any, Any, Any, Any, Throwable, Chunk[T], Unit] =
        Try(it.next()) match {
          case Success(value) if it.hasNext                             =>
            go(buffer += value, Some(value))
          case Success(value)                                           =>
            val chunk = (buffer += value).result()
            updateLast(Some(value)) *> ZChannel.write(chunk)
          case Failure(reason: ReplyDocumentIteratorExhaustedException) =>
            updateLast(lastValue) *> ZChannel.write(buffer.result()) *> ZChannel.fail(reason)
          case Failure(reason @ CursorOps.UnrecoverableException(_))    =>
            updateLast(lastValue) *> ZChannel.write(buffer.result()) *> ZChannel.fail(reason)
          case Failure(reason)                                          =>
            updateLast(lastValue) *> ZChannel.write(buffer.result()) *> handleError(lastValue, reason)
        }

      def updateLast(latest: Option[T]) = latest match {
        case None        => ZChannel.unit
        case v @ Some(_) => ZChannel.fromZIO(state.update(_.map(_.copy(_2 = v))))
      }

      def handleError(lastValue: Option[T], reason: Throwable) =
        ZChannel.unwrap(errorHandler(lastValue, reason) map {
          case Cursor.Cont(current @ Some(_)) =>
            ZChannel.fromZIO(state.update(_.map(_.copy(_2 = current)))) *> reader(current)
          case Cursor.Cont(_)                 => reader(lastValue)
          case Done(_)                        => ZChannel.unit
          case Cursor.Fail(error)             => ZChannel.fail(error)
          case x                              =>
            ZChannel.failCause(
              Cause.die(
                new IllegalArgumentException(
                  s"$x: Should never come here, but 'match may not be exhaustive'"
                )
              )
            )
        })

      reader(None)
    }

  private def fromResponse(r: Response)(implicit trace: Trace) =
    ZChannel.succeed(wrappee.documentIterator(r).take(maxDocs - r.reply.startingFrom))

}

private[zio] object ResponseUnroller {
  def make[T](
    wrappee: WithOps[T],
    maxDocs: Int,
    last: Ref[Option[(Response, Option[T])]],
    errorHandler: ZErrorHandler[Option[T]]
  )(implicit
    trace: Trace
  ): ZIO[Any, Nothing, ResponseUnroller[T]] =
    ZIO.succeed(new ResponseUnroller[T](wrappee, maxDocs, last, errorHandler))
}

private[zio] case class CursorDriver[T](
  last: IO[Option[Nothing], (Response, Option[T])],
  next: Response => ZChannel[Any, Any, Any, Any, Throwable, Chunk[T], Unit],
  reset: UIO[Unit],
  kill: Response => Task[Unit]
) {
  def killAndReset(implicit trace: Trace): ZIO[Any, Nothing, Unit] =
    last.unsome.flatMap {
      case Some((r, _)) => kill(r).ignoreLogged *> reset
      case _            => ZIO.unit
    }

  def pullDocuments(implicit trace: Trace): ZChannel[Any, Throwable, Response, Any, Throwable, Chunk[T], Unit] = {
    lazy val go: ZChannel[Any, Throwable, Response, Any, Throwable, Chunk[T], Unit] = ZChannel.readWith(
      (r: Response) => next(r) *> go,
      (e: Throwable) => ZChannel.fail(e),
      (_: Any) => ZChannel.unit
    )

    go
  }
}

private[zio] object CursorDriver {
  def make[T](
    last: Ref[Option[(Response, Option[T])]],
    more: Response => ZChannel[Any, Any, Any, Any, Throwable, Chunk[T], Unit],
    kill: Response => Task[Unit]
  )(implicit trace: Trace): ZIO[Scope, Nothing, CursorDriver[T]] =
    ZIO
      .succeed(
        CursorDriver(
          last = last.get.some,
          next = (r: Response) => ZChannel.unwrap(last.set(Some((r, None))) as more(r)),
          reset = last.set(None),
          kill = kill
        )
      )
      .withFinalizer(_.killAndReset)
}
