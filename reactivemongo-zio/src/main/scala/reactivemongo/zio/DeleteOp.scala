package reactivemongo.zio

import reactivemongo.api.bson.BSONDocument
import reactivemongo.api.bson.collection.BSONSerializationPack
import reactivemongo.api.bson.collection.BSONSerializationPack.Writer
import reactivemongo.api.commands.WriteResult
import reactivemongo.api.{ Accessors, Collation }
import reactivemongo.core.errors.ReactiveMongoException
import reactivemongo.zio.MongoOverrideContext.writeConcernWith
import reactivemongo.zio.results.MultiBulkWriteResult

import zio.{ Trace, URIO, ZIO }

final case class DeleteOp private[zio] (
  private val ordered: Boolean
) {
  val builder: URIO[BZONCollection, BZONCollection#DeleteBuilder] =
    writeConcernWith(wc => ZIO.serviceWith[BZONCollection](b => wc.fold(b.delete(ordered))(b.delete(ordered, _))))

  def one[Q](q: Q, limit: Option[Int] = None, collation: Option[Collation] = None)(implicit
    qw: Writer[Q]
  ): ZIO[BZONCollection, ReactiveMongoException, WriteResult]                                 =
    builder
      .flatMap(b =>
        ZIO
          .fromFuture(implicit ec => b.one(q, limit, collation))
      )
      .refineOrDie { case e: ReactiveMongoException => e }

  /**
   * Prepares an [[DeleteElement]].
   *
   * @param q         A query selector document
   * @param limit     The maximum number of documents to delete
   * @param collation The optional collation specification
   * @see [[many]]
   */
  def element[Q](q: Q, limit: Option[Int] = None, collation: Option[Collation] = None)(implicit
    qw: Writer[Q],
    trace: Trace
  ): ZIO[Any, ReactiveMongoException, DeleteElement]                                          =
    ZIO
      .attempt(BSONSerializationPack.serialize(q, qw))
      .map { query =>
        new DeleteElement(query, limit.getOrElse(0), collation)
      }
      .refineOrDie { case e: ReactiveMongoException => e }

  def many(
    deletes: Iterable[DeleteElement]
  )(implicit trace: Trace): ZIO[BZONCollection, ReactiveMongoException, MultiBulkWriteResult] =
    writeConcernWith(wc =>
      ZIO
        .serviceWithZIO[BZONCollection](c =>
          ZIO
            .fromFuture(implicit ec =>
              wc.fold(c.delete(ordered))(c.delete(ordered, _))
                .many(deletes.map(_.toElement(c)))
            )
            .map(MultiBulkWriteResult.apply)
            .refineOrDie { case e: ReactiveMongoException =>
              e
            }
        )
    )
}

final class DeleteElement(
  val q: BSONDocument,
  val limit: Int,
  val collation: Option[Collation]
) {

  private[zio] def toElement(coll: BZONCollection) =
    Accessors.deleteElement(coll)(q, limit, collation)
}
