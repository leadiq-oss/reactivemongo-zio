package reactivemongo.zio

import reactivemongo.api.{ Collection, CollectionProducer }

import zio.{ Trace, ZIO }

case class CollectionName(name: String) {
  def toCollection[C <: Collection: CollectionProducer](implicit trace: Trace): ZIO[Mongo, Throwable, C] =
    ZIO.serviceWithZIO[Mongo](_.getCollection[C](name))
}
