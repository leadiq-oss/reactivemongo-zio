package reactivemongo.zio

import reactivemongo.api.bson.collection.BSONCollectionProducer

import zio.stream.ZStream
import zio._

case class ZCollection(mongo: Mongo, collection: CollectionName) {

  def apply[R, E, A](query: => ZIO[BZONCollection & R, E, A])(implicit trace: Trace): ZIO[R, E, A] =
    query.provideSomeLayer[R](
      ZLayer(mongo.getCollection[BZONCollection](collection.name).orDie)
    )

  def apply[R, E, A](stream: => ZStream[BZONCollection & R, E, A])(implicit trace: Trace): ZStream[R, E, A] =
    stream.provideSomeLayer[R](ZLayer(mongo.getCollection[BZONCollection](collection.name).orDie))

}
