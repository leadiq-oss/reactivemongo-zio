package reactivemongo.zio

import reactivemongo.api.bson.collection.BSONSerializationPack
import reactivemongo.api.commands.WriteResult
import reactivemongo.api.indexes.{ CollectionIndexesManager, Index }
import reactivemongo.zio.ZIndexesManager.IndexAux

import zio.{ Task, Trace, ZIO }

class ZIndexesManager private (coll: ZIndexesManager.Aux) {

  def list()(implicit trace: Trace): Task[List[IndexAux]] =
    ZIO.fromFuture(_ => coll.list())

  def ensure(index: IndexAux)(implicit trace: Trace): Task[Boolean] =
    ZIO.fromFuture(_ => coll.ensure(index))

  def create(index: IndexAux)(implicit trace: Trace): Task[WriteResult] =
    ZIO.fromFuture(_ => coll.create(index))

  def drop(indexName: String)(implicit trace: Trace): Task[Int] =
    ZIO.fromFuture(_ => coll.drop(indexName))

  def dropAll()(implicit trace: Trace): Task[Int] =
    ZIO.fromFuture(_ => coll.dropAll())

}

object ZIndexesManager {
  def make(implicit trace: Trace): ZIO[BZONCollection, Nothing, ZIndexesManager] =
    ZIO.serviceWithZIO[BZONCollection](coll =>
      ZIO.executor.map(ex => new ZIndexesManager(coll.indexesManager(ex.asExecutionContext)))
    )

  private[zio] type IndexAux = Index { type Pack = BSONSerializationPack.type }
  private[zio] type Aux      = CollectionIndexesManager { type Pack = BSONSerializationPack.type }
}
