package reactivemongo.zio

import reactivemongo.api.Cursor.WithOps
import reactivemongo.api.bson.collection.BSONSerializationPack.Reader
import reactivemongo.api.{ Accessors, CursorOptions }
import reactivemongo.zio.MongoOverrideContext.overrideMongoContext
import reactivemongo.zio.aggregation.{ PipelineOperator, ZAggregatorContext }

import zio.stream.ZStream
import zio.{ NonEmptyChunk, Trace, ZIO }

trait Aggregations {

  def aggregateWith[T: Reader](
    explain: Boolean = false,
    allowDiskUse: Boolean = false,
    bypassDocumentValidation: Boolean = false,
    batchSize: Option[Int] = None
  )(pipeline: NonEmptyChunk[PipelineOperator])(implicit
    trace: Trace
  ): ZStream[BZONCollection, Throwable, T] =
    ZStream.unwrap(
      for {
        coll            <- ZIO.service[BZONCollection]
        ctx             <- overrideMongoContext.get
        aggregateCursor <- ZIO.attempt {
                             val aggregateCursor: WithOps[T] =
                               ZAggregatorContext.make[T](
                                 coll,
                                 pipeline.toList,
                                 explain = explain,
                                 allowDiskUse = allowDiskUse,
                                 bypassDocumentValidation = bypassDocumentValidation,
                                 readConcern = ctx.readConcern.getOrElse(Accessors.readConcern(coll)),
                                 readPreference = ctx.readPreference.getOrElse(coll.readPreference),
                                 writeConcern = ctx.writeConcern.getOrElse(Accessors.writeConcern(coll)),
                                 batchSize = batchSize,
                                 cursorOptions = CursorOptions.empty,
                                 maxTime = None,
                                 reader = implicitly[Reader[T]],
                                 hint = None,
                                 comment = None,
                                 collation = None
                               )

                             streams.cursorProducer.produce(aggregateCursor).documentStream()
                           }
      } yield aggregateCursor
    )

}
