package reactivemongo.zio.aggregation

import scala.concurrent.duration.FiniteDuration

import reactivemongo.api._
import reactivemongo.api.bson.collection.BSONSerializationPack.{ Reader, Writer }
import reactivemongo.api.bson.collection.{ BSONCollection, BSONSerializationPack }
import reactivemongo.api.commands.{
  AggregationFramework => AggFramework,
  CollectionCommand,
  Command,
  CommandCodecs,
  CommandKind,
  CommandWithPack,
  CommandWithResult,
  ResolvedCollectionCommand
}
import reactivemongo.core.protocol.MongoWireVersion
import reactivemongo.zio.aggregation.ZAggregator.{ AggregateCmd, commandWriter }

/** The [[https://docs.mongodb.com/manual/core/aggregation-pipeline/ aggregation framework]] for this collection */
object ZAggregationFramework
    extends AggFramework[BSONSerializationPack.type]
    with PackSupport[BSONSerializationPack.type] {

  val pack: BSONSerializationPack.type = BSONSerializationPack
}

// ---

/** Aggregator context */
final class ZAggregatorContext[T] private[zio] (
  val collection: BSONCollection,
  val pipeline: List[PipelineOperator],
  val explain: Boolean,
  val allowDiskUse: Boolean,
  val bypassDocumentValidation: Boolean,
  val readConcern: ReadConcern,
  val writeConcern: WriteConcern,
  val readPreference: ReadPreference,
  val batchSize: Option[Int],
  val cursorOptions: CursorOptions,
  val maxTime: Option[FiniteDuration],
  val reader: Reader[T],
  val hint: Option[BSONHint],
  val comment: Option[String],
  val collation: Option[Collation]
) {

  def prepared: ZAggregator[T] =
    new ZAggregator[T](this)
}

object ZAggregatorContext {

  def make[T](
    coll: BSONCollection,
    pipeline: List[PipelineOperator],
    explain: Boolean,
    allowDiskUse: Boolean,
    bypassDocumentValidation: Boolean,
    readConcern: ReadConcern,
    writeConcern: WriteConcern,
    readPreference: ReadPreference,
    batchSize: Option[Int],
    cursorOptions: CursorOptions,
    maxTime: Option[FiniteDuration],
    reader: Reader[T],
    hint: Option[BSONHint],
    comment: Option[String],
    collation: Option[Collation]
  ): Cursor.WithOps[T] = {
    val context = new ZAggregatorContext[T](
      coll,
      pipeline,
      explain = explain,
      allowDiskUse = allowDiskUse,
      bypassDocumentValidation = bypassDocumentValidation,
      readConcern = readConcern,
      readPreference = readPreference,
      writeConcern = writeConcern,
      batchSize = batchSize,
      cursorOptions = cursorOptions,
      maxTime = maxTime,
      reader = reader,
      hint = hint,
      comment = comment,
      collation = collation
    )

    context.prepared.cursor
  }

}

private[zio] final class ZAggregator[T] private[reactivemongo] (
  val context: ZAggregatorContext[T]
) {

  private lazy val runner = Command.run(context.collection.pack, context.collection.failoverStrategy)

  private def ver = Accessors.connectionState(context.collection.db).metadata.maxWireVersion

  def cursor: Cursor.WithOps[T] = {
    def batchSz                                  = context.batchSize.getOrElse(101)
    implicit def writer: Writer[AggregateCmd[T]] = commandWriter[T](context.collection)
    implicit def aggReader: Reader[T]            = context.reader

    val cmd = new ZAggregate[T](
      context.pipeline,
      context.explain,
      context.allowDiskUse,
      batchSz,
      ver,
      context.bypassDocumentValidation,
      context.readConcern,
      context.writeConcern,
      context.hint,
      context.comment,
      context.collation
    )

    val cursor = runner.cursor[T, ZAggregate[T]](
      context.collection,
      cmd,
      context.cursorOptions,
      context.readPreference,
      context.maxTime.map(_.toMillis)
    )

    cursor
  }
}

object ZAggregator {
  type AggregateCmd[T] = ResolvedCollectionCommand[ZAggregate[T]]

  private final def commandWriter[T](coll: BSONCollection): Writer[AggregateCmd[T]] = {
    val builder: SerializationPack.Builder[BSONSerializationPack.type] = BSONSerializationPack.newBuilder
    val version                                                        = Accessors.connectionState(coll.db).metadata.maxWireVersion
    val session                                                        = coll.db.session.filter(_ => (version.compareTo(MongoWireVersion.V36) >= 0))

    val writeWriteConcern = CommandCodecs.writeWriteConcern(builder)
    val writeCollation    = Accessors.serializeWith(builder)(_)

    BSONSerializationPack.writer[AggregateCmd[T]] { agg =>
      import agg.{ command => cmd }
      import builder.{ boolean, document, elementProducer => element }

      val pipeline = builder.array(cmd.pipeline.map(_.makePipe))

      lazy val isOut: Boolean = cmd.pipeline.lastOption.exists {
        case _: ZAggregationFramework.Out => true
        case _                            => false
      }

      val writeReadConcern =
        CommandCodecs.writeSessionReadConcern(builder)(session)

      val elements = Seq.newBuilder[BSONSerializationPack.ElementProducer]

      elements ++= Seq(
        element("aggregate", builder.string(agg.collection)),
        element("pipeline", pipeline),
        element("explain", boolean(cmd.explain)),
        element("allowDiskUse", boolean(cmd.allowDiskUse)),
        element("cursor", document(Seq(element("batchSize", builder.int(cmd.batchSize)))))
      )

      if (cmd.wireVersion >= MongoWireVersion.V32) {
        elements += element("bypassDocumentValidation", boolean(cmd.bypassDocumentValidation))

        elements ++= writeReadConcern(cmd.readConcern)
      }

      cmd.comment.foreach { comment =>
        elements += element("comment", builder.string(comment))
      }

      cmd.hint.map(_.nameOrSpec).foreach {
        case Right(str) =>
          elements += element("hint", builder.string(str))

        case Left(doc) =>
          elements += element("hint", doc)
      }

      cmd.collation.foreach { collation =>
        elements += element("collation", writeCollation(collation))
      }

      if (cmd.wireVersion >= MongoWireVersion.V36 && isOut) {
        elements += element("writeConcern", writeWriteConcern(cmd.writeConcern))
      }

      document(elements.result())
    }
  }
}

/**
 * @param pipeline the sequence of MongoDB aggregation operations
 * @param explain specifies to return the information on the processing of the pipeline
 * @param allowDiskUse enables writing to temporary files
 * @param batchSize the batch size
 * @param bypassDocumentValidation available only if you specify the \$out aggregation operator
 * @param readConcern the read concern (since MongoDB 3.2)
 */
private[zio] final class ZAggregate[T](
  val pipeline: Seq[PipelineOperator],
  val explain: Boolean = false,
  val allowDiskUse: Boolean,
  val batchSize: Int,
  val wireVersion: MongoWireVersion,
  val bypassDocumentValidation: Boolean,
  val readConcern: ReadConcern,
  val writeConcern: WriteConcern,
  val hint: Option[BSONHint],
  val comment: Option[String],
  val collation: Option[Collation]
) extends CollectionCommand
    with CommandWithPack[BSONSerializationPack.type]
    with CommandWithResult[T] {
  protected[reactivemongo] val commandKind = CommandKind.Aggregate
}
