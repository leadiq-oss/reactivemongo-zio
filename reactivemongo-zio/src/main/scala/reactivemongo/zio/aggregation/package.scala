package reactivemongo.zio

package object aggregation {

  /**
   * Aggregation pipeline operator/stage
   */
  final type PipelineOperator = ZAggregationFramework.PipelineOperator
}
