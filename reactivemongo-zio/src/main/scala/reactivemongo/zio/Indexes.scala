package reactivemongo.zio

import reactivemongo.api.commands.WriteResult
import reactivemongo.zio.ZIndexesManager.IndexAux
import zio._

trait Indexes {

  def indexesManagerWith[R, E, A](f: ZIndexesManager => ZIO[R, E, A])(implicit
    trace: Trace
  ): ZIO[R & BZONCollection, E, A] =
    ZIndexesManager.make.flatMap(f)

  def listIndexes()(implicit trace: Trace): ZIO[BZONCollection, Throwable, List[IndexAux]] =
    indexesManagerWith[Any, Throwable, List[IndexAux]](_.list())

  def ensureIndex(index: IndexAux)(implicit trace: Trace): ZIO[BZONCollection, Throwable, Boolean] =
    indexesManagerWith[Any, Throwable, Boolean](_.ensure(index))

  def createIndex(index: IndexAux)(implicit trace: Trace): ZIO[BZONCollection, Throwable, WriteResult] =
    indexesManagerWith[Any, Throwable, WriteResult](_.create(index))

  def dropIndex(indexName: String)(implicit trace: Trace): ZIO[BZONCollection, Throwable, Int] =
    indexesManagerWith[Any, Throwable, Int](_.drop(indexName))

  def dropAllIndexes()(implicit trace: Trace): ZIO[BZONCollection, Throwable, Int] =
    indexesManagerWith[Any, Throwable, Int](_.dropAll())

}
