package reactivemongo.zio

import reactivemongo.api.bson.BSONDocument
import reactivemongo.api.bson.collection.BSONCollection
import reactivemongo.api.bson.collection.BSONSerializationPack.Writer
import reactivemongo.api.{ Collation, ReadConcern }

import zio.Duration

case class QueryBuilder(builder: BSONCollection#QueryBuilder) extends AnyVal {

  @inline def projection[Pjn](p: Pjn)(implicit writer: Writer[Pjn]): QueryBuilder =
    copy(builder.projection(p))

  @inline def projection(document: BSONDocument): QueryBuilder =
    copy(builder.projection(document))

  @inline def skip(n: Int): QueryBuilder =
    copy(builder.skip(n))

  @inline def sort(document: BSONDocument): QueryBuilder =
    copy(builder.sort(document))

  @inline def explain(flag: Boolean = true): QueryBuilder =
    copy(builder.explain(flag))

  @inline def snapshot(flag: Boolean = true): QueryBuilder =
    copy(builder.snapshot(flag))

  @inline def comment(message: String): QueryBuilder =
    copy(builder.comment(message))

  @inline def maxTime(d: Duration): QueryBuilder =
    copy(builder.maxTimeMs(d.toMillis))

  @inline def maxAwaitTime(d: Duration): QueryBuilder =
    copy(builder.maxAwaitTimeMs(d.toMillis))

  @inline def singleBatch(flag: Boolean = true): QueryBuilder =
    copy(builder.singleBatch(flag))

  @inline def readConcern(concern: ReadConcern): QueryBuilder =
    copy(builder.readConcern(concern))

  @inline def maxScan(max: Double): QueryBuilder =
    copy(builder.maxScan(max))

  @inline def returnKey(flag: Boolean = true): QueryBuilder =
    copy(builder.returnKey(flag))

  @inline def showRecordId(flag: Boolean = true): QueryBuilder =
    copy(builder.showRecordId(flag))

  @inline def max(document: BSONDocument): QueryBuilder =
    copy(builder.max(document))

  @inline def min(document: BSONDocument): QueryBuilder =
    copy(builder.min(document))

  @inline def collation(collation: Collation): QueryBuilder =
    copy(builder.collation(collation))

  @inline def awaitData: QueryBuilder =
    copy(builder.awaitData)

  @inline def batchSize(n: Int): QueryBuilder =
    copy(builder.batchSize(n))

  @inline def exhaust: QueryBuilder =
    copy(builder.exhaust)

  @inline def noCursorTimeout: QueryBuilder =
    copy(builder.noCursorTimeout)

  @inline def allowPartialResults: QueryBuilder =
    copy(builder.allowPartialResults)

  @inline def slaveOk: QueryBuilder =
    copy(builder.slaveOk)

  @inline def tailable: QueryBuilder =
    copy(builder.tailable)
}
