package reactivemongo.zio

import scala.concurrent.duration.FiniteDuration

import reactivemongo.api._
import reactivemongo.api.bson.collection.BSONSerializationPack.Writer
import reactivemongo.api.bson.{ BSONDocument, document }
import reactivemongo.core.errors.ReactiveMongoException
import reactivemongo.zio.MongoOverrideContext.{ mongoContextWith, writeConcernWith }
import reactivemongo.zio.results.FindAndModifyResult

import zio.{ Trace, ZIO }

trait Queries {

  def findWith[S](query: S)(f: QueryBuilder => QueryBuilder)(implicit
    sw: Writer[S]
  ): ZQueryBuilder =
    ZQueryBuilder(ZIO.serviceWith[BZONCollection](c => f(QueryBuilder(c.find(query))).builder))

  def findAll: ZQueryBuilder =
    findWith(document)(identity)

  def find[S](query: S)(implicit sw: Writer[S]): ZQueryBuilder =
    findWith(query)(identity)

  def find[S, J](
    query: S,
    projection: Option[J] = None
  )(implicit sw: Writer[S], jw: Writer[J]): ZQueryBuilder =
    findWith(query)(b => projection.foldLeft(b)(_.projection(_)))(sw)

  lazy val insert: InsertOp =
    insert()

  def insert(ordered: Boolean = false): InsertOp =
    InsertOp(ordered)

  lazy val delete: DeleteOp =
    delete()

  def delete(ordered: Boolean = false): DeleteOp =
    DeleteOp(ordered)

  lazy val update: UpdateOp =
    update()

  def update(ordered: Boolean = false, bypassDocumentValidation: Boolean = false): UpdateOp =
    UpdateOp(ordered, bypassDocumentValidation)

  def count(limit: Option[Int])(implicit trace: Trace): ZIO[BZONCollection, ReactiveMongoException, Long] =
    mongoContextWith(ctx =>
      for {
        coll <- ZIO.service[BZONCollection]
        c    <- ZIO.fromFuture(implicit ec =>
                  coll.count(
                    None,
                    limit,
                    readConcern = ctx.readConcern.getOrElse(Accessors.readConcern(coll)),
                    readPreference = ctx.readPreference.getOrElse(coll.readPreference)
                  )
                )
      } yield c
    ).refineOrDie { case e: ReactiveMongoException => e }

  def count[S](
    query: S,
    limit: Option[Int] = None,
    skip: Int = 0,
    hint: Option[BSONHint] = None
  )(implicit sw: Writer[S], trace: Trace): ZIO[BZONCollection, ReactiveMongoException, Long]              =
    mongoContextWith(ctx =>
      for {
        coll <- ZIO.service[BZONCollection]
        q    <- ZIO.fromTry(sw.writeTry(query))
        c    <- ZIO.fromFuture(implicit ec =>
                  coll.count(
                    Some(q),
                    limit,
                    skip,
                    hint.map(_.toHint(coll)),
                    ctx.readConcern.getOrElse(Accessors.readConcern(coll)),
                    ctx.readPreference.getOrElse(coll.readPreference)
                  )
                )
      } yield c
    ).refineOrDie { case e: ReactiveMongoException => e }

  def findAndUpdate[S, T](
    selector: S,
    update: T,
    fetchNewObject: Boolean = false,
    upsert: Boolean = false,
    sort: Option[BSONDocument] = None,
    fields: Option[BSONDocument] = None,
    bypassDocumentValidation: Boolean = false,
    maxTime: Option[FiniteDuration] = None,
    collation: Option[Collation] = None,
    arrayFilters: Seq[BSONDocument] = Seq.empty
  )(implicit
    swriter: Writer[S],
    writer: Writer[T],
    trace: Trace
  ): ZIO[BZONCollection, Throwable, FindAndModifyResult]                                                  =
    writeConcernWith { wc =>
      ZIO
        .service[BZONCollection]
        .flatMap { coll =>
          ZIO.fromFuture(implicit ec =>
            coll.findAndUpdate(
              selector,
              update,
              fetchNewObject = fetchNewObject,
              upsert = upsert,
              sort = sort,
              fields = fields,
              bypassDocumentValidation = bypassDocumentValidation,
              writeConcern = wc.getOrElse(Accessors.writeConcern(coll)),
              maxTime = maxTime,
              collation = collation,
              arrayFilters = arrayFilters
            )
          )
        }
        .map(FindAndModifyResult.apply)
    }

  def findAndRemove[S](
    selector: S,
    sort: Option[BSONDocument] = None,
    fields: Option[BSONDocument] = None,
    maxTime: Option[FiniteDuration] = None,
    collation: Option[Collation] = None,
    arrayFilters: Seq[BSONDocument] = Seq.empty
  )(implicit
    swriter: Writer[S],
    trace: Trace
  ): ZIO[BZONCollection, Throwable, FindAndModifyResult] =
    writeConcernWith { wc =>
      ZIO
        .serviceWithZIO[BZONCollection] { coll =>
          ZIO.fromFuture(implicit ec =>
            coll.findAndRemove(
              selector,
              sort,
              fields,
              writeConcern = wc.getOrElse(Accessors.writeConcern(coll)),
              maxTime,
              collation,
              arrayFilters
            )
          )
        }
        .map(FindAndModifyResult.apply)
    }

}
