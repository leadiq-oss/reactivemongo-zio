package reactivemongo.zio

import reactivemongo.api.bson.BSONObjectID
import reactivemongo.api.{ Session => _, _ }
import reactivemongo.zio.Mongo.{ SessionPartiallyApplied, TransactionPartiallyApplied }
import reactivemongo.zio.MongoOverrideContext.failoverStrategyWith
import reactivemongo.zio.transactions.{ Session, Transactions }
import zio.stream.{ ZStream, ZStreamAspect }
import zio._

trait Mongo { self =>

  /**
   * Returns the underlying DB reference of the current mongo connection
   */
  def database(implicit trace: Trace): Task[DB]

  /**
   * Returns a new collection reference for the connection with the provided name and using
   * the provided failover strategy.
   */
  def getCollection[C <: Collection: CollectionProducer](
    name: String
  )(implicit trace: Trace): Task[C] =
    failoverStrategyWith(fs => database.map(db => fs.fold(db.collection[C](name))(db.collection[C](name, _))))

  def collection(name: String): ZCollection =
    collection(CollectionName(name))

  def collection(collectionName: CollectionName): ZCollection =
    ZCollection(self, collectionName)

  @deprecated("Use `collection(name).apply() instead", "2.0.0-RC4")
  def collectionWithZIO[R, E, A](name: CollectionName)(
    query: => ZIO[BZONCollection & R, E, A]
  )(implicit trace: Trace): ZIO[R, E, A] =
    collection(name).apply[R, E, A](query)

  @deprecated("Use `collection(name).apply() instead", "2.0.0-RC4")
  def collectionWithZIO[R, E, A](name: String)(
    query: => ZIO[BZONCollection & R, E, A]
  )(implicit trace: Trace): ZIO[R, E, A] =
    collectionWithZIO[R, E, A](CollectionName(name))(query)

  @deprecated("Use `collection(name).apply() instead", "2.0.0-RC4")
  def collectionWithStream[R, E, A](name: CollectionName)(stream: => ZStream[BZONCollection & R, E, A])(implicit
    trace: Trace
  ): ZStream[R, E, A] =
    collection(name).apply[R, E, A](stream)

  @deprecated("Use `collection(name).apply() instead", "2.0.0-RC4")
  def collectionWithStream[R, E, A](name: String)(
    stream: => ZStream[BZONCollection & R, E, A]
  )(implicit
    trace: Trace
  ): ZStream[R, E, A] =
    collectionWithStream[R, E, A](CollectionName(name))(stream)

  /**
   * Generates a new BSONObjectID with with only the timestamp set.
   */
  def objectId(fillOnlyTimestamp: Boolean)(implicit trace: Trace): ZIO[Any, Nothing, BSONObjectID] =
    Mongo.objectId(fillOnlyTimestamp)

  /**
   * Generates a new BSONObjectID
   */
  def objectId(implicit trace: Trace): ZIO[Any, Nothing, BSONObjectID] = objectId(fillOnlyTimestamp = false)

  /**
   * Runs an effect that requires a mongo in the context of a session.
   */
  final def session[R]: SessionPartiallyApplied[R] =
    new SessionPartiallyApplied[R](self)

  /**
   * Runs the provided result within the context of a mongo transaction, which allows multiple queries
   * to be executed atomically.
   */
  final def transaction[R]: TransactionPartiallyApplied[R] =
    new TransactionPartiallyApplied[R](self)

}

object Mongo
    extends Transactions
    with Aggregations
    with Queries
    with Collections
    with Indexes
    with Meta
    with Modifiers
    with MongoConstructors
    with reactivemongo.zio.DistinctOps {

  object aspects {

    def readPreference(preference: ReadPreference): ZIOAspect[Nothing, Any, Nothing, Any, Nothing, Any] =
      MongoAspect.readPreference(preference)

    def readConcern(concern: ReadConcern): ZIOAspect[Nothing, Any, Nothing, Any, Nothing, Any] =
      MongoAspect.readConcern(concern)

    def writeConcern(concern: WriteConcern): ZIOAspect[Nothing, Any, Nothing, Any, Nothing, Any] =
      MongoAspect.writeConcern(concern)

    def failoverStrategy(strategy: => FailoverStrategy): ZIOAspect[Nothing, Any, Nothing, Any, Nothing, Any] =
      MongoAspect.failoverStrategy(strategy)

    def readPreference(preference: ReadPreference)(implicit
      dummy: DummyImplicit
    ): ZStreamAspect[Nothing, Any, Nothing, Any, Nothing, Any] =
      MongoStreamAspect.readPreference(preference)

    def readConcern(concern: ReadConcern)(implicit
      dummy: DummyImplicit
    ): ZStreamAspect[Nothing, Any, Nothing, Any, Nothing, Any] =
      MongoStreamAspect.readConcern(concern)

    def writeConcern(concern: WriteConcern)(implicit
      dummy: DummyImplicit
    ): ZStreamAspect[Nothing, Any, Nothing, Any, Nothing, Any] =
      MongoStreamAspect.writeConcern(concern)

    def failoverStrategy(strategy: => FailoverStrategy)(implicit
      dummy: DummyImplicit
    ): ZStreamAspect[Nothing, Any, Nothing, Any, Nothing, Any] =
      MongoStreamAspect.failoverStrategy(strategy)

  }

  def database: ZIO[Mongo, Throwable, DB] =
    ZIO.serviceWithZIO[Mongo](_.database)

  def objectId(fillOnlyTimestamp: Boolean)(implicit trace: Trace): ZIO[Any, Nothing, BSONObjectID] =
    ObjectIdFactory.generate(fillOnlyTimestamp)

  def objectId(implicit trace: Trace): ZIO[Any, Nothing, BSONObjectID] =
    objectId(fillOnlyTimestamp = false)

  final class TransactionPartiallyApplied[R](private val mongo: Mongo) extends AnyVal {
    def apply[E, A](
      effect: => ZIO[Mongo & Session & R, E, A]
    )(implicit trace: Trace): ZIO[R & Session, E, A] =
      Session.makeTransaction[R, E, A](effect).provideSomeEnvironment[R & Session](_.add[Mongo](mongo))
  }

  final class SessionPartiallyApplied[R](private val mongo: Mongo) extends AnyVal {
    def apply[E, A](
      effect: => ZIO[Session & R, E, A]
    )(implicit trace: Trace, tag: Tag[Session]): ZIO[R, E, A] =
      ZIO.scoped[R](
        Session.make.orDie
          .flatMap(session => effect.provideSomeEnvironment[R](_.add[Session](session)))
          .provideSomeEnvironment[R & Scope](_.add[Mongo](mongo))
      )
  }

  private[zio] case class Proxy(mongo: Mongo, db: DB) extends Mongo {
    def database(implicit trace: Trace): Task[DB] = ZIO.succeed(db)

  }
}
