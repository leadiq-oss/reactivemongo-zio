package reactivemongo.api

import reactivemongo.api.bson.BSONDocument
import reactivemongo.api.bson.collection.BSONSerializationPack
import reactivemongo.api.collections.GenericCollection
import zio._

final case class BSONHint(nameOrSpec: Either[BSONDocument, String]) {

  def toHint[C <: GenericCollection[BSONSerializationPack.type] & CollectionMetaCommands](
    coll: C
  ): coll.Hint =
    nameOrSpec.fold(doc => coll.hint(doc), s => coll.hint(s))
}
