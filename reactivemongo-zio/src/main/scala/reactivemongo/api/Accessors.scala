package reactivemongo.api

import reactivemongo.api.bson.BSONDocument
import reactivemongo.api.bson.collection.{ BSONCollection, BSONSerializationPack }
import reactivemongo.api.collections.GenericCollection
import reactivemongo.zio.aggregation.ZAggregationFramework

object Accessors {

  @inline def readConcern[C <: GenericCollection[_]](coll: C) =
    coll.db.connection.options.readConcern

  @inline def writeConcern[C <: GenericCollection[_]](coll: C) =
    coll.db.connection.options.writeConcern

  @inline def connectionState(db: DB): ConnectionState =
    db.connectionState

  @inline def connectionOptions(connection: MongoConnection): MongoConnectionOptions =
    connection.options

  @inline def defaultReadConcern: ReadConcern =
    ReadConcern.default

  def serializeWith(builder: SerializationPack.Builder[BSONSerializationPack.type])(
    collation: Collation
  ): BSONSerializationPack.Document =
    Collation.serializeWith(BSONSerializationPack, collation)(builder)

  def deleteElement(c: BSONCollection)(
    q: BSONDocument,
    limit: Int,
    collation: Option[Collation]
  ) = new c.DeleteElement(q, limit, collation)

  @inline def changeStream(
    offset: Option[ZAggregationFramework.ChangeStream.Offset],
    fullDocumentStrategy: Option[ChangeStreams.FullDocumentStrategy]
  ) =
    new ZAggregationFramework.ChangeStream(offset, fullDocumentStrategy)
}
