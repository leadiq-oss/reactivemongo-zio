package reactivemongo.zio

import zio.{ Scope, Tag, Trace, ZIO, ZLayer }

private[zio] trait MongoConstructorsVersionSpecific { self: MongoConstructors =>

  /**
   * Returns a layer which constructs a new Mongo instance using the underlying connection, the database name is specified by part of the environment which can be extracted
   * using the provided function
   */
  def layerEnvironment[C: Tag](f: C => String)(implicit trace: Trace): ZLayer[Connection with C, Throwable, Mongo] =
    ZLayer(makeEnvironment(f))

  /**
   * Produces a scoped effect which produces a mongo instance. The provided function is used to
   * extract the connection string from the environment.
   *
   * @see [[fromEnvironmentConfigScoped]]
   * @see [[fromConnectionStringScoped]]
   */
  def fromEnvironmentConfigScoped[C](
    f: C => String
  )(implicit tagged: Tag[C], trace: Trace): ZIO[Scope with C, Throwable, Mongo] =
    ZIO.serviceWithZIO[C](c => fromConnectionStringScoped(f(c)))

  @deprecated("Use fromEnvironmentConfigScoped instead", "2.0.0-RC4")
  def fromConfigStringScoped[C](
    f: C => String
  )(implicit tagged: Tag[C], trace: Trace): ZIO[Scope with C, Throwable, Mongo] =
    fromEnvironmentConfigScoped[C](f)

  @deprecated("Use fromEnvironmentConfig instead", "2.0.0-RC4")
  def fromConfigString[C](
    f: C => String
  )(implicit tagged: Tag[C], trace: Trace): ZLayer[C, Throwable, Mongo] =
    fromEnvironmentConfig[C](f)

  /**
   * Produces a new mongo layer given a connection string. The provided function is used to extract
   * the connection string from the environment.
   */
  def fromEnvironmentConfig[C](
    f: C => String
  )(implicit tagged: Tag[C], trace: Trace): ZLayer[C, Throwable, Mongo] =
    ZLayer.scoped[C](ZIO.service[C].flatMap(c => fromConnectionStringScoped(f(c))))

  /**
   * Constructs a new Mongo instance using the underlying connection, the database name is specified by part of the environment which can be extracted
   * using the provided function
   */
  def makeEnvironment[C: Tag](f: C => String)(implicit trace: Trace): ZIO[Connection with C, Throwable, Mongo] =
    ZIO.serviceWithZIO[C](c => make(f(c)))

}
