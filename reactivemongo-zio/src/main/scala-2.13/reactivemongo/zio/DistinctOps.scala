package reactivemongo.zio

import scala.collection.Factory

import reactivemongo.api.bson.BSONDocument
import reactivemongo.api.bson.collection.BSONSerializationPack.NarrowValueReader
import reactivemongo.api.{ Accessors, Collation, ReadConcern }
import reactivemongo.core.errors.ReactiveMongoException

import zio.{ Trace, ZIO }

trait DistinctOps {
  def distinct[T, M[_] <: Iterable[_]](
    key: String,
    selector: Option[BSONDocument] = None,
    readConcern: Option[ReadConcern] = None,
    collation: Option[Collation] = None
  )(implicit
    reader: NarrowValueReader[T],
    cbf: Factory[T, M[T]],
    trace: Trace
  ): ZIO[BZONCollection, ReactiveMongoException, M[T]] =
    ZIO
      .serviceWithZIO[BZONCollection] { coll =>
        ZIO.fromFuture { implicit ec =>
          coll.distinct[T, M](
            key,
            selector,
            readConcern.getOrElse(Accessors.readConcern(coll)),
            collation = collation
          )
        }
      }
      .refineOrDie { case e: ReactiveMongoException => e }
}
