package zio.mongo

import java.util.concurrent.TimeUnit
import reactivemongo.api.bson.BSONObjectID
import reactivemongo.zio.{ Mongo, ObjectIdFactory }
import zio.test.{ ZIOSpecDefault, assertTrue }
import zio._
import zio.test.{ Spec, TestEnvironment }

object ObjectIdSpec extends ZIOSpecDefault {

  val timeOnlyGenerator: ObjectIdFactory = new ObjectIdFactory {
    def generate(fillOnlyTimestamp: Boolean)(implicit trace: Trace): UIO[BSONObjectID] =
      Clock.currentTime(TimeUnit.MILLISECONDS).map(BSONObjectID.fromTime(_, true))
  }

  val spec: Spec[Environment & TestEnvironment & Scope, Any] = suite("objectId")(
    test("override generate - scoped")(
      for {
        _   <- ObjectIdFactory.withObjectIdScoped(timeOnlyGenerator)
        id1 <- Mongo.objectId
        id2 <- Mongo.objectId
      } yield assertTrue(id1 == id2)
    ),
    test("override scoped - regional")(
      for {
        _   <- ZIO.scoped(ObjectIdFactory.withObjectIdScoped(timeOnlyGenerator))
        id1 <- Mongo.objectId
        id2 <- Mongo.objectId
      } yield assertTrue(id1 != id2)
    ),
    test("override generate - locally")(
      ObjectIdFactory.withObjectId(timeOnlyGenerator)(
        (Mongo.objectId zipWith Mongo.objectId)((id1, id2) => assertTrue(id1 == id2))
      )
    ),
    test("no override generate")(
      for {
        id1 <- Mongo.objectId
        id2 <- Mongo.objectId
      } yield assertTrue(id1 != id2)
    )
  )

}
