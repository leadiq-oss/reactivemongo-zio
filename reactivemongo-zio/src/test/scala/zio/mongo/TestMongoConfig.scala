package zio.mongo

import zio.Config
import Config._
import zio.config._

final case class TestMongoConfig(useTestContainer: Boolean, url: String)

object TestMongoConfig {
  val descriptor: Config[TestMongoConfig] =
    (boolean("useTestContainer") zip string("url")).to[TestMongoConfig]
}
