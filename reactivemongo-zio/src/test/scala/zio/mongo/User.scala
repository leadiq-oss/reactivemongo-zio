package zio.mongo

import reactivemongo.api.bson.Macros.Annotations.Key
import reactivemongo.api.bson.{ BSONDocumentHandler, Macros }

case class User(@Key("_id") id: Int, name: Option[String] = None, age: Int = 42)

object User {
  implicit val handler: BSONDocumentHandler[User] = Macros.handler[User]
}
