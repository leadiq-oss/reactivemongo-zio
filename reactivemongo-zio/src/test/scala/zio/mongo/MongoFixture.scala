package zio.mongo

import com.dimafeng.testcontainers.MongoDBContainer
import org.testcontainers.utility.DockerImageName
import reactivemongo.api.AsyncDriver
import reactivemongo.api.bson.collection.BSONCollectionProducer
import reactivemongo.zio.{ BZONCollection, CollectionName, Connection, Mongo, MongoDriver }
import zio.config.typesafe._
import zio.test.TestAspect
import zio.{ Config, Random, Scope, URIO, ZEnvironment, ZIO, ZLayer }

object MongoFixture {

  val driver = MongoDriver.layer

  private val testMongoContainer: ZIO[Scope, Nothing, MongoDBContainer] = ZIO
    .fromAutoCloseable(
      ZIO.succeed(MongoDBContainer(DockerImageName.parse("mongo:6.0.11")))
    )
    .tap(container => ZIO.succeed(container.start))

  val mongoConnectionUrl: ZLayer[TestMongoConfig, Nothing, TestMongoConfig] =
    ZLayer.scoped(
      ZIO.serviceWithZIO[TestMongoConfig](testConfig =>
        if (testConfig.useTestContainer)
          testMongoContainer.map(container => testConfig.copy(url = container.replicaSetUrl))
        else ZIO.succeed(testConfig)
      )
    )

  val mongoConfigLayer: ZLayer[Any, Config.Error, TestMongoConfig] =
    ZLayer(
      ZIO.withConfigProvider(TypesafeConfigProvider.fromResourcePath())(
        ZIO.config(TestMongoConfig.descriptor.nested("mongo"))
      )
    )

  val connectionLayer: ZLayer[TestMongoConfig with MongoDriver, Throwable, Connection] =
    Connection.fromEnvironmentConfig[TestMongoConfig](_.url)

  val testMongo: ZLayer[Any, Nothing, Mongo with AsyncDriver with TestMongoConfig] =
    ZLayer
      .make[Mongo with AsyncDriver with TestMongoConfig](
        driver,
        (mongoConfigLayer >>> mongoConnectionUrl),
        connectionLayer,
        Mongo.layer
      )
      .orDie

  private val availableCharacters = "abcdefghijklmnopqrstuvwxyz".toVector

  private val collectionName =
    ZIO
      .collectAll(ZIO.replicate(5)(Random.nextIntBounded(26).map(availableCharacters)))
      .map(_.mkString)

  val collection: ZLayer[Mongo, Nothing, BZONCollection] = ZLayer.scoped {
    (for {
      name <- collectionName.map("test-run-" + _)
      coll <- CollectionName(name)
                .toCollection[BZONCollection]
                .withFinalizer(coll => Mongo.drop().provideEnvironment(ZEnvironment(coll)).orDie)
                .orDie
    } yield coll)
  }

  def addUser(id: Int): URIO[BZONCollection, Unit] =
    Mongo.insert.one(User(id, age = 42)).ignore

  def drop(coll: String): TestAspect[Nothing, Mongo with Any, Nothing, Any] = {
    import reactivemongo.zio.toMongoSyntax
    TestAspect.before(Mongo.drop().collection(coll).orDie)
  }
}
