package zio.mongo
import reactivemongo.api.indexes.{ Index, IndexType }
import reactivemongo.zio.Mongo

import zio.Scope
import zio.test.{ Spec, TestEnvironment, TestResult, assertTrue }

object IndexSpec extends MongoITSpec {
  def spec: Spec[TestEnvironment with Mongo with Scope, Any] =
    suite("indexes")(
      test("createIndex") {
        val index = Index(List("name" -> IndexType.Ascending), Some("name_1"))

        Mongo.collectionWithZIO[Any, Throwable, TestResult]("indexes") {
          for {
            _       <- Mongo.createIndex(index)
            created <- Mongo.ensureIndex(index)
            stats   <- Mongo.stats
            deleted <- Mongo.dropIndex("name_1")
            after   <- Mongo.stats
          } yield assertTrue(!created, stats.nindexes == 2, deleted == 2, after.nindexes == 1)
        }
      }
    )
}
