package zio.mongo

import reactivemongo.zio.{ Connection, MongoDriver }

import zio.mongo.MongoFixture.{ mongoConfigLayer, mongoConnectionUrl }
import zio.test.{ TestAspect, TestSystem, ZIOSpec, assertCompletes, testEnvironment }
import zio.{ ZIO, ZLayer }
import zio.Chunk
import zio.test.{ TestAspectAtLeastR, TestEnvironment }

object ConnectionSpec extends ZIOSpec[MongoDriver with TestMongoConfig] {
  override val aspects: Chunk[TestAspectAtLeastR[Environment with TestEnvironment]] =
    super.aspects ++ List(TestAspect.sequential)

  override val bootstrap = testEnvironment >+> ZLayer.make[MongoDriver with TestMongoConfig](
    MongoDriver.layer,
    mongoConfigLayer >>> mongoConnectionUrl
  )

  def spec = suite("ConnectionSpec")(
    test("use config provider")(
      ZIO.scoped(for {
        config <- ZIO.service[TestMongoConfig]
        _      <- TestSystem.putEnv("MONGO_URI", config.url)
        _      <- Connection.fromConfigScoped
      } yield assertCompletes)
    )
  )
}
