package zio.mongo

import reactivemongo.api.Cursor
import reactivemongo.api.Cursor.ErrorHandler
import reactivemongo.api.bson.{ BSONDocumentReader, BSONDocumentWriter, document }
import reactivemongo.zio.ZCursor.ZErrorHandler
import reactivemongo.zio._
import zio.stream.{ Take, ZSink, ZStream }
import zio.test.Assertion._
import zio.test.TestAspect.before
import zio.test._
import zio._

object ZCursorSpec extends MongoITSpec with ZCursorCompatSpec {

  import MongoFixture._

  def badUser(id: Int): URIO[BZONCollection, Unit] = Mongo.insert().one(document("_id" -> id, "age" -> "_bad")).ignore

  def spec: Spec[ITEnv & TestEnvironment, Any] =
    (suite("ZCursor")(
      test("empty cursor")(
        assertZIO(Mongo.find(document("_id" -> 42)).stream[User](-1).runCollect)(
          isEmpty
        )
      ),
      test("# docs < maxDocs")(assertZIO(Mongo.find(document).stream[User](11).runCollect)(hasSize(equalTo(10)))),
      test("# docs > maxDocs")(
        assertZIO(Mongo.find(document).stream[User](5).runCollect)(hasSize(equalTo(5)))
      ),
      test("tailable cursors")(
        Mongo.collectionWithZIO("tailable") {
          for {
            _                  <- Mongo.createCapped(2, None)
            queue              <- Queue.unbounded[Take[Throwable, User]]
            fiber              <- Mongo.findWith(document)(_.tailable.awaitData).stream[User]().take(10).runIntoQueue(queue).fork
            resultsBefore      <- queue.takeAll
            _                  <- ZIO.foreachDiscard(1 to 100)(i => Mongo.insert.one(User(i, Some(i.toString))))
            unflattenedResults <- fiber.join *> queue.takeAll
            resultsAfter       <- ZStream.fromChunk(unflattenedResults).flattenTake.runCollect
          } yield assert(resultsAfter)(hasSize(equalTo(10))) && assertTrue(resultsBefore.isEmpty)
        }
      ) @@ TestAspect.before(Mongo.drop().collection("tailable")),
      test("default limit")(
        assertZIO(Mongo.find(document).stream[User]().runCollect)(hasSize(equalTo(10)))
      ),
      test("large # of docs")(
        for {
          result <- Mongo.find(document).stream[User]().runCollect

        } yield {
          val size = result.size
          assertTrue(size == 1000)
        }
      ) @@ before(ZIO.foreachDiscard(11 to 1000)(addUser)),
      suite("Error handling")(
        akkaSpec,
        test("FailOnError - Fold") {
          assertZIO(runAllAsFold(document, Cursor.FailOnError[Chunk[User]]()).exit)(
            fails(hasMessage(equalTo("Fails to handle 'age': BSONString != BSONInteger")))
          )
        },
        test("FailOnError - ZIO")(
          assertZIO(runAllAsZIO(document, ZCursor.FailOnError[Option[User]]()).exit)(
            dies(
              hasMessage(
                equalTo(
                  "Fails to handle 'age': BSONString != BSONInteger"
                )
              )
            )
          )
        ),
        test("DoneOnError - Fold") {
          assertZIO(runAllAsFold(document, Cursor.DoneOnError[Chunk[User]]()))(
            hasSameElements[User](
              Chunk.fromIterable(List.iterate(User(1), 10)(u => u.copy(id = u.id + 1)))
            )
          )
        },
        test("DoneOnError - ZIO")(
          assertZIO(runAllAsZIO(document, ZCursor.DoneOnError[Option[User]]()))(
            hasSameElements[User](
              Chunk.fromIterable(List.iterate(User(1), 10)(u => u.copy(id = u.id + 1)))
            )
          )
        ),
        test("ContOnError - ZIO")(
          assertZIO(
            runAllAsZIO(document, ZCursor.ContOnError[Option[User]]())
          )(
            hasSize(equalTo(19))
          )
        ),
        test("ContOnError - Fold") {
          assertZIO(runAllAsFold(document, Cursor.ContOnError[Chunk[User]]()))(
            hasSize(equalTo(19))
          )
        }
      ) @@ before(badUser(11) *> ZIO.foreachDiscard(12 to 20)(addUser)),
      suite("Connection")(
        suite("Error handling")(
          test("continues on error") {
            ZIO.scoped(
              for {
                finalizerAndConn <- Connection.fromEnvironmentConfigScoped[TestMongoConfig](_.url).withEarlyRelease
                (finalizer, conn) = finalizerAndConn
                mongo            <- Mongo.make("test").provideEnvironment(ZEnvironment(conn))
                collection        = mongo.collection("test")
                error            <- Ref.make[Option[Throwable]](None)
                result           <- collection {
                                      ZIO.foreachDiscard(0 to 100)(addUser) *>
                                        Mongo
                                          .findWith(document)(_.batchSize(3))
                                          .stream[User](
                                            err = ZCursor.ContOnError { (_, e) =>
                                              error.set(Some(e)).unit
                                            }
                                          )
                                          .zipWithIndex
                                          .tap { case (_, i) => ZIO.when(i == 2)(finalizer) }
                                          .run(ZSink.count)
                                    }
                e                <- error.get
              } yield assertTrue(
                e.get.getMessage
                  .contains("MongoError['This MongoConnection is closed"),
                result == 3
              )
            )
          }
        )
      )
    ) @@ before(ZIO.foreachDiscard(1 to 10)(addUser)))
      .provideSomeLayer[ITEnv & TestEnvironment](collection) @@ TestAspect.timeout(30.seconds)

  def runAllAsFold[A: BSONDocumentWriter, T: BSONDocumentReader](
    query: A,
    err: ErrorHandler[Chunk[T]]
  ): ZIO[BZONCollection, Throwable, Chunk[T]] =
    for {
      collection <- ZIO.service[BZONCollection]
      result     <-
        ZIO
          .fromFuture(implicit ec => collection.find(query).cursor[T]().collect[Chunk](maxDocs = 20, err))
    } yield result

  def runAllAsZIO[A: BSONDocumentWriter, T: BSONDocumentReader](
    query: A,
    err: ZErrorHandler[Option[T]]
  ): ZIO[BZONCollection, Throwable, Chunk[T]] =
    Mongo
      .find(query)
      .stream[T](20, err)
      .runCollect
}
