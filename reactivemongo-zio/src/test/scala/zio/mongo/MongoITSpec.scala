package zio.mongo

import reactivemongo.api.AsyncDriver
import reactivemongo.zio.Mongo
import zio.internal.stacktracer.Tracer
import zio.mongo.MongoFixture.testMongo
import zio.test.{ Spec, TestAspect, TestAspectAtLeastR, TestEnvironment, ZIOSpec, testEnvironment }
import zio._

abstract class MongoITSpec extends ZIOSpec[Mongo & AsyncDriver & TestMongoConfig] {
  type ITEnv = Mongo with AsyncDriver with TestMongoConfig

  override def aspects: Chunk[TestAspectAtLeastR[ITEnv & TestEnvironment]] =
    super.aspects ++ Chunk(TestAspect.sequential, TestAspect.withLiveEnvironment)

  override val bootstrap: ZLayer[Any, Any, ITEnv & TestEnvironment] = MongoITSpec.layer

  def dropCollections(name: String, names: String*): TestAspect[Nothing, Mongo, Throwable, Any] =
    TestAspect.after(
      Mongo.collectionWithZIO(name)(Mongo.drop()) *> ZIO.foreachDiscard(names)(
        Mongo.collectionWithZIO(_)(Mongo.drop())
      )
    )

  def spec: Spec[TestEnvironment & ITEnv & Scope, Any]
}

object MongoITSpec {

  private val layer: ZLayer[Any, Any, Mongo & AsyncDriver & TestMongoConfig & TestEnvironment] = {
    implicit val trace: zio.Trace = Tracer.newTrace
    testEnvironment >+> testMongo
  }

}
