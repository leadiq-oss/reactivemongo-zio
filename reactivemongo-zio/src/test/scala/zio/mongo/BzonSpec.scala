package zio.mongo

import reactivemongo.api.bson.{ BSONDocumentHandler, BSONString, Macros, document }
import reactivemongo.api.indexes.{ Index, IndexType }
import reactivemongo.zio.Mongo._
import reactivemongo.zio.{ BZONCollection, Mongo, _ }

import zio.mongo.MongoFixture.addUser
import zio.test.Assertion._
import zio.test.TestAspect.before
import zio.test.{ assertCompletes, assertCompletesZIO, assertTrue, assertZIO }
import zio.{ NonEmptyChunk, ZLayer }

object BzonSpec extends MongoITSpec {
  case class Foo(_id: Int)

  implicit val handler: BSONDocumentHandler[Foo] = Macros.handler[Foo]

  def spec = suite("Queries")(
    suite("collection")(
      test("regionalization") {
        for {
          exists       <- collectionWithZIO("regional-collection1") {
                            insert.one(document("_id" -> 42)).collection("regional-collection1") *>
                              find(document("_id" -> 42)).one[Foo]
                          }
          doesNotExist <- find(document("_id" -> 42)).one[Foo].collection("regional-collection2")
        } yield assertTrue(exists.get == Foo(42)) && assertTrue(doesNotExist.isEmpty)
      } @@ dropCollections("regional-collection1", "regional-collection2"),
      test("eliminate collection") {
        (Mongo.objectId
          .flatMap(id => find(document("_id" -> id)).one[Foo]) <* findAll.one[Foo])
          .collection("eliminated") *> assertCompletesZIO
      },
      test("zcollection")(
        for {
          collection <- Mongo.collection(CollectionName("zcollection"))
          _          <- collection(findAll.one[Foo])
          _          <- collection(findAll.stream[Foo]()).runDrain
        } yield assertCompletes
      )
    ),
    suite("find")(
      test("empty one")(
        assertZIO(find(document("_id" -> 42)).one[User])(
          isNone
        )
      ),
      test("exists one")(
        assertZIO(addUser(42) *> find(document("_id" -> 42)).one[User])(
          isSome(equalTo(User(42)))
        )
      )
    ),
    suite("update")(
      test("upsert one")(
        assertZIO(update.one(User(42), User(42, Some("Jon")), upsert = true) *> find(User(42)).one[User])(
          isSome(equalTo(User(42, Some("Jon"))))
        )
      ),
      test("no update")(
        assertZIO(update.one(User(42), User(42, Some("Jon"))).map(_.nModified))(
          equalTo(0) ?? "# of expected updates"
        )
      ),
      test("update failed")(
        assertZIO(update.one(User(42), User(43)).flip.map(_.message))(
          isOneOf(
            Seq(
              "The _id field cannot be changed from {_id: 42} to {_id: 43}.",
              "After applying the update, the (immutable) field '_id' was found to have been altered to _id: 43"
            )
          )
        )
      ) @@ before(addUser(42))
    ),
    suite("count")(
      test("count - all")(
        assertZIO(
          insert.many(
            List.fill(10)(document("a" -> "b"))
          ) *> count(None)
        )(equalTo(10L))
      ),
      test("count - filtered")(
        assertZIO(
          insert.many(
            List.fill(5)(document("a" -> "b")) ++ List.fill(5)(document("a" -> "c"))
          ) *> count(document("a" -> "b"))
        )(equalTo(5L))
      ),
      test("count - limit")(
        assertZIO(
          insert.many(List.fill(10)(document("a" -> "b"))) *>
            count(Some(5))
        )(equalTo(5L))
      )
    ),
    test("delete")(
      assertZIO(
        insert.many(List.fill(10)(document("a" -> "b"))) *>
          delete.one(document("a" -> "b")) *>
          count(document("a" -> "b"))
      )(equalTo(0L))
    ),
    test("add index")(
      assertZIO(
        indexesManagerWith(im =>
          im.create(
            Index(
              List("value" -> IndexType.Ascending)
            )
          ) *> im.list()
        )
      )(hasSize(equalTo(2)))
    ).provideSomeLayer[BZONCollection](ZLayer(ZIndexesManager.make)),
    suite("Aggregation")(
      test("simple pipeline") {
        import reactivemongo.zio.aggregation.ZAggregationFramework._
        assertZIO(
          insert.many(
            List(
              document("cust_id" -> "A123", "amount" -> 500, "status" -> "A"),
              document("cust_id" -> "A123", "amount" -> 250, "status" -> "A"),
              document("cust_id" -> "B212", "amount" -> 200, "status" -> "A"),
              document("cust_id" -> "A123", "amount" -> 300, "status" -> "D")
            )
          ) *>
            Mongo
              .aggregateWith[AggModel]()(
                NonEmptyChunk(
                  Match(document("status" -> "A")),
                  Group(BSONString("$cust_id"))("total" -> SumField("amount"))
                )
              )
              .runCollect
        )(
          hasSameElements(
            List(
              AggModel("B212", 200),
              AggModel("A123", 750)
            )
          )
        )
      }
    )
  ).provideSomeLayer[Mongo](MongoFixture.collection)

  case class AggModel(_id: String, total: Int)

  object AggModel {
    implicit val handler: BSONDocumentHandler[AggModel] = Macros.handler[AggModel]
  }
}
