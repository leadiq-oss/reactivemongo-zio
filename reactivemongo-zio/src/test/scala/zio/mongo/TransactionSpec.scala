package zio.mongo

import reactivemongo.api.bson.collection.BSONCollectionProducer
import reactivemongo.api.bson.{ BSONDocument, document }
import reactivemongo.zio.Mongo._
import reactivemongo.zio.{ Mongo, _ }

import zio.test.Assertion.{ anything, equalTo, isSome }
import zio.test.{ TestAspect, assertCompletesZIO, assertTrue, assertZIO }
import zio.{ Promise, ZIO, ZLayer, durationInt }

object TransactionSpec extends MongoITSpec {
  val spec = suite("Transactions")(
    test("attempt transaction") {
      assertZIO(
        session {
          transaction {
            collectionWithZIO("users") {
              insert.one(document("_id" -> 42)) *>
                find(document("_id" -> 42)).one[BSONDocument]
            }
          }
        }
      )(anything)
    },
    test("transaction isn't visible before commit") {
      for {
        lock   <- Promise.make[Nothing, Unit]
        fiber  <- session {
                    transaction {
                      insert
                        .one(document("_id" -> 42))
                        .collection("users") *> lock.await
                    }
                  }.fork
        query   = find(document("_id" -> 42)).one[BSONDocument]
        first  <- query
        second <- lock.succeed(()) *> fiber.join *> query
      } yield assertTrue(first.isEmpty, second.nonEmpty)
    },
    test("aborted transactions are never visible") {
      val query = collectionWithZIO("users")(find(document("_id" -> 43)).one[BSONDocument])

      for {
        before <- query
        _      <- session {
                    transaction {
                      insert
                        .one(document("_id" -> 43))
                        .collection("users") *> ZIO
                        .fail(new Exception("Boom"))
                    }
                  }.ignore
        after  <- query
      } yield assertTrue(before.isEmpty, after.isEmpty)

    },
    test("interleaving") {
      session {
        session {
          transaction {
            insert.one(document("_id" -> 42)).collection("users")
          }
        } <* insert.one(document("_id" -> 43)).collection("users")
      } *> assertCompletesZIO
    },
    test("query visible within transaction") {
      val result = session {
        findAll.one[BSONDocument] <*> transaction {
          insert.one(document("_id" -> 42)) *>
            findAll.one[BSONDocument] <* transaction {
              insert.one(document)
            }
        }
      }

      result.map { case (before, after) =>
        assertTrue(before.isEmpty, after.nonEmpty)
      }
    },
    test("unify nested transactions") {
      session {
        transaction {
          (insert.one(document("_id" -> 42, "v" -> 1)) *>
            transaction(
              update.one(
                document("_id"  -> 42),
                document("$inc" -> document("v" -> 2))
              )
            )).collection("users")
        }
      } *> assertZIO(
        find(document("_id" -> 42))
          .one[BSONDocument]
          .map(
            _.flatMap(_.get("v").flatMap(_.asOpt[Int]))
          )
      )(isSome(equalTo(3)))
    },
    test("only allow single inflight transaction per session") {
      session {
        Promise.make[Nothing, Unit].flatMap { latch =>
          val t1 = transaction(insert.one(document("_id" -> 42, "v" -> 1)).collection("users") *> latch.await)
          val t2 = transaction(insert.one(document("_id" -> 42)).collection("users") *> latch.await)
          (t1 race t2).fork.flatMap(s => latch.succeed(()).delay(3.seconds) *> s.join)
        }
      } *> assertCompletesZIO
    }
  ).@@(MongoFixture.drop("users"))
    .provideSomeLayer(
      ZLayer(Mongo.getCollection[BZONCollection]("users").orDie)
    ) @@ TestAspect.withLiveEnvironment @@ TestAspect.ifEnvNotSet("IS_CI")
}
