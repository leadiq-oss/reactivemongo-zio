package zio.mongo

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.Materializer
import reactivemongo.api.Cursor
import reactivemongo.api.Cursor.ErrorHandler
import reactivemongo.api.bson.{ BSONDocument, BSONDocumentReader, BSONDocumentWriter, document }
import reactivemongo.zio.BZONCollection
import zio.test.Assertion._
import zio.test._
import zio.{ Chunk, ZIO }

trait ZCursorCompatSpec { self: MongoITSpec =>

  val akkaSpec = suite("Pekko Streams")(
    test("FailOnError - Pekko") {
      assertZIO(runAllAsPekko(document, Cursor.FailOnError[Option[User]]()).exit)(
        fails(hasMessage(equalTo("Fails to handle 'age': BSONString != BSONInteger")))
      )
    },
    test("DoneOnError - Pekko") {
      assertZIO(runAllAsPekko[BSONDocument, User](document, Cursor.DoneOnError()))(
        hasSameElements[User](
          // Pekko appears to be buggy, it will resend the last value if `DoneOnError` is selected
          Chunk.fromIterable(List.iterate(User(1), 10)(u => u.copy(id = u.id + 1)) :+ User(10))
        )
      )
    }
  )

  private def runAllAsPekko[A: BSONDocumentWriter, T: BSONDocumentReader](
    query: A,
    err: ErrorHandler[Option[T]]
  ): ZIO[BZONCollection, Throwable, Chunk[T]] = {
    import reactivemongo.pekkostream.cursorProducer
    ZIO.scoped(ZIO.executorWith { executor =>
      ZIO.acquireRelease(
        ZIO.succeed(
          ActorSystem(
            name = "reactivemongo-pekkostream-cursor",
            defaultExecutionContext = Some(executor.asExecutionContext)
          )
        )
      )(system => ZIO.fromFuture(_ => system.terminate()).ignore)
    }.flatMap { implicit actorSystem =>
      implicit val materializer = Materializer(actorSystem)

      for {
        collection <- ZIO.service[BZONCollection]
        result     <-
          ZIO.fromFuture(implicit ec =>
            collection
              .find(query)
              .cursor[T]()
              .documentSource(maxDocs = 11, err)
              .runFold[Chunk[T]](Chunk.empty)(_ :+ _)
          )
      } yield result
    })
  }

}
