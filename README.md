# reactivemongo-zio

| Release | Snapshot | Discord |
| --- | --- | --- |
| [![Release Artifacts][Badge-SonatypeReleases]][Link-SonatypeReleases] | [![Snapshot Artifacts][Badge-SonatypeSnapshots]][Link-SonatypeSnapshots] | [![Badge-Discord]][Link-Discord] |

# Summary
[ZIO](https://zio.dev/) wrapper for [reactivemongo](http://reactivemongo.org/) for purely functional and composable MongoDB access. reactivemongo-zio interacts seamlessly with ZIO and ZIO-Streams.

# Documentation
[reactivemongo-zio Microsite (In progress)](https://leadiq-oss.gitlab.io/reactivemongo-zio/)

# Getting started

[Installation](docs/overview/index.md)
[Basic Operations](docs/overview/basic-operations.md)

# Contributing
[Documentation for contributors](docs/about/contributing.md)

## Code of Conduct

See the [Code of Conduct](docs/about/code_of_conduct.md)

## Support

Come chat with us on [![Badge-Discord]][Link-Discord].


# License
[License](LICENSE)

[Badge-SonatypeReleases]: https://img.shields.io/nexus/r/https/s01.oss.sonatype.org/com.leadiq/reactivemongo-zio_2.12.svg "Sonatype Releases"
[Badge-SonatypeSnapshots]: https://img.shields.io/nexus/s/https/s01.oss.sonatype.org/com.leadiq/reactivemongo-zio_2.12.svg "Sonatype Snapshots"
[Badge-Discord]: https://img.shields.io/discord/629491597070827530?logo=discord "chat on discord"
[Link-SonatypeReleases]: https://oss.sonatype.org/content/repositories/releases/com/leadiq/reactivemongo-zio_2.12/ "Sonatype Releases"
[Link-SonatypeSnapshots]: https://oss.sonatype.org/content/repositories/snapshots/com/leadiq/reactivemongo-zio_2.12/ "Sonatype Snapshots"
[Link-Discord]: https://discord.gg/2ccFBr4 "Discord"

