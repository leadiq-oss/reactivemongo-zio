import BuildHelper.*

inThisBuild(
  List(
    organization := "com.leadiq",
    homepage := Some(url("https://gitlab/leadiq-oss/reactivemongo-zio/")),
    scmInfo := Some(
      ScmInfo(
        url("https://gitlab.com/leadiq-oss/reactivemongo-zio"),
        "scm:git@gitlab.com:leadiq-oss/reactivemongo-zio.git"
      )
    ),
    licenses := List("Apache-2.0" -> url("http://www.apache.org/licenses/LICENSE-2.0")),
    developers := List(
      Developer(
        "paulpdaniels",
        "Paul Daniels",
        "paul@leadiq.com",
        url("https://leadiq.com")
      )
    )
  )
)

ThisBuild / gitVersioningSnapshotLowerBound := "1.999.0"

addCommandAlias("fmt", "all scalafmtSbt scalafmt test:scalafmt")
addCommandAlias("fix", "; all compile:scalafix test:scalafix; all scalafmtSbt scalafmtAll")
addCommandAlias("check", "; scalafmtSbtCheck; scalafmtCheckAll; compile:scalafix --check; test:scalafix --check")

val zioVersion                = "2.1.14"
val reactiveMongoVersion      = "1.1.0-pekko.RC14"
val reactiveMongoPekkoVersion = "1.1.0-RC14"
val refinedVersion            = "0.11.1"
val testContainerVersion      = "0.41.8"
val zioConfigVersion          = "4.0.3"

lazy val root = project
  .in(file("."))
  .settings(
    publish / skip := true
  )
  .aggregate(
    reactivemongoZio,
    acolyteZio,
    refined
  )

lazy val reactivemongoZio = (project in file("reactivemongo-zio"))
  .settings(stdSettings("reactivemongo-zio"))
  .settings(buildInfoSettings("reactivemongo.zio"))
  .settings(
    resolvers ++= Resolver.sonatypeOssRepos("snapshots"),
    ThisBuild / libraryDependencies ++= Seq(
      "dev.zio"           %% "zio"                          % zioVersion,
      "dev.zio"           %% "zio-streams"                  % zioVersion,
      "org.reactivemongo" %% "reactivemongo"                % reactiveMongoVersion,
      "org.reactivemongo" %% "reactivemongo-pekkostream"    % reactiveMongoPekkoVersion % Test,
      "dev.zio"           %% "zio-test"                     % zioVersion                % Test,
      "dev.zio"           %% "zio-test-sbt"                 % zioVersion                % Test,
      "dev.zio"           %% "zio-config-typesafe"          % zioConfigVersion          % Test,
      "com.dimafeng"      %% "testcontainers-scala-mongodb" % testContainerVersion      % Test
    )
  )
  .settings(
    testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework"),
    Test / fork := true
  )

lazy val refined = (project in file("refined"))
  .settings(stdSettings("reactivemongo-zio-refined", versions = Seq(BuildHelper.Scala212, BuildHelper.Scala213)))
  .settings(buildInfoSettings("reactivemongo.zio.refined"))
  .settings(
    libraryDependencies ++= Seq(
      "eu.timepit" %% "refined"      % refinedVersion % Provided,
      "dev.zio"    %% "zio-test"     % zioVersion     % Test,
      "dev.zio"    %% "zio-test-sbt" % zioVersion     % Test
    )
  )
  .dependsOn(reactivemongoZio)

lazy val acolyteZio = (project in file("acolyte-zio"))
  .dependsOn(reactivemongoZio)
  .settings(stdSettings("acolyte-zio"))
  .settings(buildInfoSettings("reactivemongo.zio"))
  .settings(
    ThisBuild / libraryDependencies ++= Seq(
      "dev.zio"           %% "zio"                 % zioVersion,
      "dev.zio"           %% "zio-streams"         % zioVersion,
      "org.reactivemongo" %% "reactivemongo"       % reactiveMongoVersion,
      "org.eu.acolyte"    %% "reactive-mongo"      % "1.2.9",
      "org.slf4j"          % "slf4j-simple"        % "2.0.16"         % Test,
      "dev.zio"           %% "zio-test"            % zioVersion       % Test,
      "dev.zio"           %% "zio-test-sbt"        % zioVersion       % Test,
      "dev.zio"           %% "zio-config-typesafe" % zioConfigVersion % Test
    )
  )
  .settings(
    testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework"),
    resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
    Test / classLoaderLayeringStrategy := ClassLoaderLayeringStrategy.ScalaLibrary,
    // FIX TODO (maybe the next version of the library will hide this compilation error)
    // [error] -- Error: api/target/shaded/scala-3.2.2/src_managed/main/velocity/DefaultBSONHandlers.scala:379:2
    // [error] undefined: new com.github.ghik.silencer.silent # -1: TermRef(TypeRef(TermRef(TermRef(TermRef(TermRef(ThisType(TypeRef(NoPrefix,module class <root>)),object com),object github),object ghik),object silencer),silent),<init>) at readTasty
    // [error] one error found
    // [error] (mediator / Compile / doc) DottyDoc Compilation Failed
    Compile / doc / sources := Seq.empty
  )

lazy val docs = project
  .in(file("reactivemongo-zio-docs"))
  .settings(
    resolvers ++= Resolver.sonatypeOssRepos("snapshots"),
    publish / skip := true,
    moduleName := "reactivemongo-zio-docs",
    scalacOptions -= "-Yno-imports",
    scalacOptions -= "-Xfatal-warnings",
    libraryDependencies ++= Seq(
      "dev.zio" %% "zio" % zioVersion
    ),
    ScalaUnidoc / unidoc / unidocProjectFilter := inProjects(root),
    ScalaUnidoc / unidoc / target := (LocalRootProject / baseDirectory).value / "website" / "static" / "api",
    cleanFiles += (target in (ScalaUnidoc, unidoc)).value,
    docusaurusCreateSite := docusaurusCreateSite.dependsOn(Compile / unidoc).value
  )
  .dependsOn(root)
  .enablePlugins(MdocPlugin, DocusaurusPlugin, ScalaUnidocPlugin)
